//
//  Contacts+CoreDataProperties.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/18/16.
//  Copyright © 2016 Softheme. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contacts+CoreDataProperties.h"

@implementation Contacts (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic comment;
@dynamic timeStart;
@dynamic creatorID;
@dynamic timeEnd;
@dynamic longitude;
@dynamic latitude;
@dynamic relationship;

@end
