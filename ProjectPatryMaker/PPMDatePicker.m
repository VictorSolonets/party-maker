//
//  PPMDatePicker.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/9/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMDatePicker.h"

@implementation PPMDatePicker

#pragma mark Methods For DataPicker ToolBar

- (IBAction) onCancelDate :(id)sender{
    if (!self.delegate && ![self.delegate respondsToSelector: @selector(clickDoneDatePicker:)]) {
        return;
    } else {
        [self.delegate performSelector:@selector(clickCancelDatePicker:) withObject:self];
    }
}
-(IBAction) onDoneDate :(id)sender {
    if (!self.delegate && ![self.delegate respondsToSelector : @selector(clickDoneDatePicker:)]) {
        return;
    } else {
        [self.delegate performSelector:@selector(clickDoneDatePicker:) withObject:self];
    }
}



@end
