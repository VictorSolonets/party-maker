//
//  NSString+ParseDate.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPMEntityParty.h"

@interface NSString (ParseDate)

+ (NSString*) parseDate : (NSNumber*) dateStart;

+ (NSString*) timeStart : (NSNumber*) party;

+ (NSString*) timeEnd : (NSNumber*) party;

+ (NSInteger) getStartTimeMinute : (PPMEntityParty*) party;

+ (NSInteger) getEndTimeMinute : (PPMEntityParty*) party;

@end
