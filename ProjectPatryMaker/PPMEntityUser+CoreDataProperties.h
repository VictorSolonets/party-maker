//
//  PPMEntityUser+CoreDataProperties.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PPMEntityUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface PPMEntityUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *userID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *parties;

@end

@interface PPMEntityUser (CoreDataGeneratedAccessors)

- (void)addPartiesObject:(NSManagedObject *)value;
- (void)removePartiesObject:(NSManagedObject *)value;
- (void)addParties:(NSSet<NSManagedObject *> *)values;
- (void)removeParties:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
