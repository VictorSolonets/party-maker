//
//  CustomViewController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/8/16.
//  Copyright © 2016 Softheme. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewController : UIViewController

-(IBAction)refresh :(id)sender;

@end
