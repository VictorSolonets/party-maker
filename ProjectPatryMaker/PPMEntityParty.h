//
//  PPMEntityParty.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "_PPMEntityParty.h"
#import <CoreData/CoreData.h>

@class PPMEntityUser;

NS_ASSUME_NONNULL_BEGIN

@interface PPMEntityParty : NSManagedObject

+ (PPMEntityParty*)fetchWithPartyID:(NSNumber*)partyID
                            context:(NSManagedObjectContext*)context;

+ (NSArray*) getAllPatiesFromUserID:(NSNumber*) userID;

//- (PPMEntityUser*) fetchUserWithID:(NSNumber*) userID;




//+ (PPMEntityParty*)createThingWithId:(NSNumber*)thingID context:(NSManagedObjectContext*)context;
//
//+ (PPMEntityParty*)fetchThingWithID:(NSNumber*)thingID context:(NSManagedObjectContext*)managedObjectContext;
//
//+ (PPMEntityParty*) fetchOrCreate:(NSNumber*) partyID withContext:(NSManagedObjectContext*) context;

@end

NS_ASSUME_NONNULL_END

#import "PPMEntityParty+CoreDataProperties.h"
