//
//  PPMPartyNotification.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/26/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyNotification.h"

@implementation PPMPartyNotification

+ (void)addNotifications:(NSArray *)parties {
    for (PPMEntityParty *party in parties) {
        [self addNotification:party];
    }
}

+ (void)addNotification:(PPMEntityParty *)party {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    NSDate *timeToParty = [[NSDate alloc] initWithTimeIntervalSince1970:([[party timeStart] doubleValue]-3600.0)];
    if( [[NSDate date] timeIntervalSinceDate:timeToParty] < 0 ) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = [NSString stringWithFormat:@"To party %@ one hour.", [party name]];
        localNotification.alertAction = @"Pool party is about to begin!";
        localNotification.fireDate = timeToParty;
        localNotification.userInfo = @{ @"party_id" :[party partyID] };
        localNotification.repeatInterval = 0;
        localNotification.category = @"LocalNotificationDefaultCategory";
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        UIMutableUserNotificationAction *doneAction = [[UIMutableUserNotificationAction alloc] init];
        doneAction.identifier = @"doneActionIdentifier";
        doneAction.destructive = NO;
        doneAction.title = @"Mark done";
        doneAction.activationMode = UIUserNotificationActivationModeBackground;
        doneAction.authenticationRequired = NO;
        UIMutableUserNotificationCategory *category = [[UIMutableUserNotificationCategory alloc] init];
        category.identifier = @"LocalNotificationDefaultCategory";
        [category setActions:@[doneAction] forContext:UIUserNotificationActionContextMinimal];
        [category setActions:@[doneAction] forContext:UIUserNotificationActionContextDefault];
        NSSet *categories = [[NSSet alloc] initWithArray:@[category]];
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings
                                                            settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:categories];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        NSLog(@"Time less then now with name - %@" , [party name]);
    }
    
}

+ (void)removeNotifications {
    UIApplication *application = [UIApplication sharedApplication];
    for (UILocalNotification *notification in [application scheduledLocalNotifications]) {
        [application cancelLocalNotification:notification];
    }
}

@end
