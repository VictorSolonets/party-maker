//
//  PPMPartyMakeViewController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/8/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyMakeViewController.h"
#import "PPMEntityParty.h"
#import "PPMDatePicker.h"
#import "PPMPartyMakerSDK.h"
#import "PPMEntityParty.h"
#import "PPMDataStore.h"
#import "NSString+ParseDate.h"
#import <CoreLocation/CoreLocation.h>

@interface PPMPartyMakeViewController () <UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, UIPageViewControllerDelegate, PPMDatePickerDelegate, PPMSubtitleCoordinateDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *saveParty;

@property (nonatomic)  PPMDatePicker *datePickerView;

@property (weak, nonatomic) IBOutlet UIView *ovalLogo;
@property (weak, nonatomic) IBOutlet UIView *ovalDate;
@property (weak, nonatomic) IBOutlet UIView *ovalName;
@property (weak, nonatomic) IBOutlet UIView *ovalStart;
@property (weak, nonatomic) IBOutlet UIView *ovalEnd;
@property (weak, nonatomic) IBOutlet UIView *ovalDescription;
@property (weak, nonatomic) IBOutlet UIView *ovalLocation;

@property (weak, nonatomic) IBOutlet UIButton *locationButton;

@property (nonatomic) UIView *ovalBack;
@property (nonatomic, weak) UIImageView *imageView;
@property (nonatomic) NSString *textViewText;

@property (nonatomic) NSString *textTimeStart;
@property (nonatomic) NSString *textTimeEnd;

@property (weak, nonatomic) IBOutlet UITextField *timeStartField;
@property (weak, nonatomic) IBOutlet UITextField *timeEndField;

@end

@implementation PPMPartyMakeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.partyID) {
        NSManagedObjectContext *context = [[PPMDataStore sharedData] mainObjectContext];
        PPMEntityParty *party = [PPMEntityParty fetchWithPartyID:self.partyID context:context];
        self.partyName.text = [party name];
        self.partySliderMaxTime.value = [NSString getEndTimeMinute:party];
        self.partySliderMinTime.value = [NSString getStartTimeMinute:party];
        self.timeStartField.text = [NSString timeStart:[party valueForKey:PPMEntityPartyAttributes.timeStart]];
        self.timeEndField.text = [NSString timeEnd:[party valueForKey:PPMEntityPartyAttributes.timeEnd]];
        self.partyDescription.text = [party comment];
        [self.btnDateChoosing setTitle:[NSString parseDate:[party valueForKey:PPMEntityPartyAttributes.timeStart]] forState:UIControlStateNormal];
        if (![party.latitude isEqualToString:@""] && party.latitude) {
            self.location = [party longitude];
            [self.locationButton setTitle:[party latitude] forState:UIControlStateNormal];
        }
        self.subtitle = [party latitude];
        self.currentPicture = [party logoID];
    }
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    [self.partyName setReturnKeyType:UIReturnKeyDone];
    self.partyName.delegate = self;
    self.partyDescription.delegate = self;
    self.timeStartField.delegate = self;
    self.timeEndField.delegate = self;
    
    [self addToolbarForKeyboard];
    
    if (self.partyID) {
        [self.partyPageControl setCurrentPage:[self.currentPicture intValue]];
        [self onPageChanged:self.partyPageControl];
    }
    [self addPageControl];
   
    [self.image removeFromSuperview];
    [self addOvalBack];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.currentPicture = [NSNumber numberWithLong:self.partyPageControl.currentPage];
    [self.ovalBack removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) setParty: (NSNumber*) partyID {
    self.partyID = partyID;
}

- (void) moveBackOval : (int) yValue {
    __block __weak PPMPartyMakeViewController *weakSelf = self;
    [UIView animateWithDuration:0.05
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.ovalBack.frame = (CGRect){self.ovalDate.frame.origin.x-1.7, yValue-1.7, 13, 13};
                     }
                     completion:^(BOOL finished){
                     }];
}

#pragma mark methods for Date Picker View

- (void) clickCancelDatePicker: (PPMDatePicker*) datePicker{
    [UIView
     animateWithDuration: 0.4
     animations:^{
         CGRect frame = self.datePickerView.frame;
         frame.origin.y = self.view.frame.size.height;
         self.datePickerView.frame = frame;
     } completion:^(BOOL finished) {
         [datePicker removeFromSuperview];
     }];
    [self.btnDateChoosing setUserInteractionEnabled:YES];
}

- (void) clickDoneDatePicker: (PPMDatePicker*) datePicker{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM.YYYY"];
    NSString *str = [dateFormatter stringFromDate:[self.datePickerView.datePicker date]];
    
    [self.btnDateChoosing setTitle:str forState:UIControlStateNormal];
    
    [UIView animateWithDuration: 0.4
                     animations:^{
                         CGRect frame = self.datePickerView.frame;
                         frame.origin.y = self.view.frame.size.height;
                         self.datePickerView.frame = frame;
                     } completion:^(BOOL finished) {
                         [datePicker removeFromSuperview];
                     }];
    [self.btnDateChoosing setUserInteractionEnabled:YES];
    
}
- (IBAction)onDateChanged :(id)sender {
    [self moveBackOval : self.ovalDate.frame.origin.y];
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PPMDatePicker class]) owner:nil options:nil];
    self.datePickerView = nibContents[0];
    self.datePickerView.frame = (CGRect){0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height/2};
    self.datePickerView.delegate = self;
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame = self.datePickerView.frame;
        frame.origin.y = self.datePickerView.frame.size.height;
        self.datePickerView.frame = frame;
    }];
    [self.view addSubview:self.datePickerView];
    [self.btnDateChoosing setUserInteractionEnabled:NO];
}

#pragma mark Methods Save

- (IBAction)onSaveButton:(id)sender {
    NSString *error;
    if ([self.btnDateChoosing.titleLabel.text isEqual:NSLocalizedStringFromTable(@"CHOOSE DATE", @"Language",nil)])
        error = NSLocalizedStringFromTable(@"You should choose a date!",@"Language",nil);
    else if ([self.partyName.text isEqualToString:@""])
        error = NSLocalizedStringFromTable(@"You should enter a party name!",@"Language",nil);
    else if (![[PPMPartyMakerSDK sharedManager] getNetworkStatus]) {
        error = NSLocalizedStringFromTable(@"Internet is missing!", @"Language",nil);;
    } else {
        /* Prepare date for save */
        
        NSString *dateString = self.btnDateChoosing.titleLabel.text;
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        
        long timeDate = [dateFromString timeIntervalSince1970];
        long startTime = (self.partySliderMinTime.value) * 60 + timeDate;
        long endTime = (self.partySliderMaxTime.value + 30) * 60 + timeDate;

        if (!self.location) {
            self.location = @"";
        }
        if (!self.subtitle) {
            self.subtitle = @"";
        }
        
        self.subtitle = [self.subtitle stringByReplacingOccurrencesOfString:@"\'" withString:@"\\'"];
        self.partyDescription.text = [self.partyDescription.text stringByReplacingOccurrencesOfString:@"\'" withString:@"\\'"];
        self.partyName.text = [self.partyName.text stringByReplacingOccurrencesOfString:@"\'" withString:@"\\'"];
        
        
        [[PPMPartyMakerSDK sharedManager] addPartyWithName:self.partyName.text
        LogoID:[NSNumber numberWithLong : self.partyPageControl.currentPage]
        Latitude:self.subtitle
        Longitude:self.location
        Comment:self.partyDescription.text
        TimeStart:startTime
        TimeEnd:endTime
        CreatorID:[[PPMDataStore sharedData] userID]
        UserID:self.partyID
        Callback:^(NSDictionary *response, NSError *error) {
                [self.navigationController popToRootViewControllerAnimated:YES];
           }];
        
        return;
    }
    
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Error!", @"Language",nil)  message:error preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style: UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
   
    [self presentViewController:alert animated:YES completion: nil];
}

- (IBAction)onCancelButton:(id)sender {
    __weak PPMPartyMakeViewController *weakSelf = self;
    [self addActionView:^(UIAlertAction * action) {
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
    } WithTitle:NSLocalizedStringFromTable(@"Warning!", @"Language",nil) Message:NSLocalizedStringFromTable(@"All you data not saved.\nAre you sure?", @"Language",nil)];
}

#pragma mark ActionView

- (void)addActionView:(void (^)())actionBlock WithTitle : (NSString*) newTitle Message : (NSString*) newMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"YES", @"Language",nil)
                                                          style:UIAlertActionStyleDefault handler:actionBlock];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"NO", @"Language",nil)  style: UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [self presentViewController:alert animated:YES completion: nil];
}

#pragma mark methods for sliders

- (IBAction) onSlideMin {
    [self moveBackOval:self.ovalStart.frame.origin.y];
    int time = self.partySliderMinTime.value;
    self.timeStartField.text = [NSString stringWithFormat:@"%02i:%02i", time/60, time - (time/60)*60];
    if (self.partySliderMinTime.value > self.partySliderMaxTime.value) {
        self.timeEndField.text = [NSString stringWithFormat:@"%02i:%02i", (time+30)/60, time + 30 - ((time+30)/60)*60 ];
        self.partySliderMaxTime.value = (self.partySliderMinTime.value);
    }
}

- (IBAction) onSlideMax {
    [self moveBackOval:self.ovalEnd.frame.origin.y];
    int time = self.partySliderMaxTime.value;
    self.timeEndField.text = [NSString stringWithFormat:@"%02i:%02i", (time+30)/60, time + 30 - ((time+30)/60)*60 ];
    if (self.partySliderMinTime.value > self.partySliderMaxTime.value) {
        self.timeStartField.text = [NSString stringWithFormat:@"%02i:%02i", time/60, time - (time/60)*60];
        self.partySliderMinTime.value = (self.partySliderMaxTime.value);
    }
}

#pragma mark Protocol Text Field

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    [self setEnableWhenEditPartyName:YES];
    
    if ([textField.restorationIdentifier isEqualToString:@"timeStart"] || [textField.restorationIdentifier isEqualToString:@"timeEnd"]) {
       
        NSString *searchedString = textField.text;
        NSRange   searchedRange = NSMakeRange(0, [searchedString length]);
        NSString *pattern = @"^(([0-1][0-9])|([2][0-3])):[0-5][0-9]$";
        NSError  *error = nil;
        
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSArray *array= [regex matchesInString:searchedString options:0 range: searchedRange];
        if (array.count < 1) {
            if ([textField.restorationIdentifier isEqualToString:@"timeStart"]) {
                textField.text = self.textTimeStart;
            } else if ([textField.restorationIdentifier isEqualToString:@"timeEnd"]) {
                textField.text = self.textTimeEnd;
            }
        } else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm"];
            NSDate *date = [dateFormatter dateFromString:textField.text];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
            NSInteger hour = [components hour];
            NSInteger minute = [components minute];
            
            if ([textField.restorationIdentifier isEqualToString:@"timeStart"]) {
                self.partySliderMinTime.value = (minute+hour*60);
                [self onSlideMin];
            } else if ([textField.restorationIdentifier isEqualToString:@"timeEnd"]) {
                self.partySliderMaxTime.value = (minute+hour*60)-30;
                [self onSlideMax];
            }
            
        }

    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.restorationIdentifier isEqualToString:@"timeStart"]) {
        self.textTimeStart = textField.text;
        [self moveBackOval:self.ovalStart.frame.origin.y];
    } else if ([textField.restorationIdentifier isEqualToString:@"timeEnd"]) {
        self.textTimeEnd = textField.text;
        [self moveBackOval:self.ovalEnd.frame.origin.y];
    } else {
        [self moveBackOval : self.ovalName.frame.origin.y];
        [self setEnableWhenEditPartyName:NO];
    }
}

#pragma mark Protocol Text View

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView; {
    [self moveBackOval : self.ovalDescription.frame.origin.y];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    __block __weak PPMPartyMakeViewController *weakSelf = self;
    self.partySliderMaxTime.enabled = self.partyScrollView.scrollEnabled = self.partyPageControl.enabled = NO;
    self.textViewText = textView.text;
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect viewFrame = CGRectMake(0, -130, self.view.frame.size.width, self.view.frame.size.height);
                         weakSelf.view.frame = viewFrame;
                     } completion:^(BOOL finished) {
                     }];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    __block __weak PPMPartyMakeViewController *weakSelf = self;
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect viewFrame = CGRectMake(0 ,64, self.view.frame.size.width, self.view.frame.size.height);
                         weakSelf.view.frame = viewFrame;
                     } completion:^(BOOL finished) {
                         
                     }];
}


#pragma mark addition methods

- (void) setEnableWhenEditPartyName:(BOOL) CHOOSE {
    self.btnDateChoosing.enabled = self.partySliderMaxTime.enabled = self.partySliderMinTime.enabled = self.partyScrollView.scrollEnabled = CHOOSE;
}

- (void) onDoneKeyboard {
    self.textViewText = self.partyDescription.text;
    self.partySliderMaxTime.enabled = self.partyScrollView.scrollEnabled = self.partyPageControl.enabled = YES;
    [self.partyDescription resignFirstResponder];
}

- (void) onCancelKeyboard {
    self.partyDescription.text = self.textViewText;
    self.partySliderMaxTime.enabled = self.partyScrollView.scrollEnabled = self.partyPageControl.enabled = YES;
    [self.partyDescription resignFirstResponder];
}

- (IBAction)getLocation:(id)sender {
    [self moveBackOval:self.ovalLocation.frame.origin.y];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainViewController" bundle:nil];
    PPMMapViewController* mapController = [ storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    mapController.location = self.location;
    mapController.locationName = self.subtitle;
    mapController.partyName = self.partyName.text;
    mapController.logoID = [NSNumber numberWithLong:self.partyPageControl.currentPage];
    mapController.delegate = self;
    [self.navigationController pushViewController:mapController animated:YES];
}


- (void) sendSubtitle:(NSString*)subtitle location:(NSString*)location {
    self.location = location;
    self.subtitle = subtitle;
    [self.locationButton setTitle:self.subtitle forState:UIControlStateNormal];
}


#pragma mark methods to PageControl and ScrollView

-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [self moveBackOval:self.ovalLogo.frame.origin.y];
}

- (void)onPageChanged:(UIPageControl *)sender {
    [self moveBackOval:self.ovalLogo.frame.origin.y];
    CGPoint contentOffset = CGPointMake(sender.currentPage  * self.partyScrollView.frame.size.width, 0);
    [self.partyScrollView setContentOffset:contentOffset animated:YES];
}

- (void)scrollViewDidEndDecelerating: (UIScrollView *)scrollView {
    NSInteger currentPage = scrollView.contentOffset.x/self.partyScrollView.frame.size.width;;
    [self.partyPageControl setCurrentPage:currentPage];
}

- (void) addPageControl {
    self.partyScrollView.delegate = self;
    UIImageView *imageViewLocal;
    for (int i = 0; i < 6 ; i++) {
        imageViewLocal = [[UIImageView alloc] initWithImage:[UIImage imageNamed:
                                                     [NSString stringWithFormat:@"PartyLogo_Small_%d", i]]];
        float xOffset = (self.partyScrollView.frame.size.width/2 - imageViewLocal.frame.size.width/2) +
                                                                i * self.partyScrollView.frame.size.width;
        imageViewLocal.frame = CGRectMake(xOffset, self.partyScrollView.frame.size.height/2 -
            imageViewLocal.frame.size.height/2, imageViewLocal.frame.size.width, imageViewLocal.frame.size.height);
        self.imageView = imageViewLocal;
        [self.partyScrollView addSubview:self.imageView];
    }
    
    self.partyScrollView.contentSize = CGSizeMake(self.partyScrollView.frame.size.width * 6, self.partyScrollView.frame.size.height);
    [self.partyPageControl setUserInteractionEnabled:YES];
    [self.partyPageControl addTarget:self action:@selector(onPageChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.partyScrollView];
    [self.view addSubview:self.partyPageControl];
}

#pragma mark ToolBar For KeyBoard

- (void) addToolbarForKeyboard {
    UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 375, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.backgroundColor = [UIColor blackColor];
    UIBarButtonItem *itemCancel = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Cancel", @"Language",nil) style:UIBarButtonItemStylePlain target:self action:@selector(onCancelKeyboard)];
    UIBarButtonItem *itemDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Done", @"Language",nil) style:UIBarButtonItemStyleDone target:self action:@selector(onDoneKeyboard)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    itemDone.tintColor = itemCancel.tintColor = [UIColor blackColor];
    toolbar.items = @[itemCancel, flexibleSpace, itemDone];
    [toolbar sizeToFit];
    self.partyDescription.inputAccessoryView = toolbar;
}

#pragma add Oval Back

- (void) addOvalBack {
    self.ovalBack = [[UIView alloc] initWithFrame:CGRectMake(self.ovalDate.frame.origin.x-1.7, self.ovalDate.frame.origin.y-1.7, 13, 13)];
    self.ovalBack.backgroundColor = [UIColor whiteColor];
    self.ovalBack.layer.cornerRadius = 6.5;
    self.ovalBack.opaque = YES;
    self.ovalBack.alpha = 0.5;
    [self.view addSubview:self.ovalBack];
}

@end
