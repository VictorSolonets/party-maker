//
//  PPMAnnotationParty.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/23/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMAnnotationParty.h"
#import "_PPMEntityParty.h"

@interface PPMAnnotationParty()



@end

@implementation PPMAnnotationParty


- (instancetype)  initWithCoordinate:(CLLocationCoordinate2D)location
                               Title:(NSString*)title
                            subtitle:(NSString*)subtitle
                              logoID:(NSNumber *)logoID {
    self = [super init];
    if (self) {
        self.coordinate = location;
        self.title = title;
        self.subtitle = subtitle;
        self.logoID = logoID;
    }
    return self;
}

- (instancetype)  initWithParty : (PPMEntityParty*) party {
    self = [super init];
    if (self) {
        self.party = party;
        self.coordinate = [self useLocationString:[party valueForKey:PPMEntityPartyAttributes.longitude]];
        self.logoID = [party valueForKey:PPMEntityPartyAttributes.logoID];
        self.subtitle = [party valueForKey:PPMEntityPartyAttributes.latitude];
        self.title = [party valueForKey:PPMEntityPartyAttributes.name];
    }
    return self;
}

- (MKPinAnnotationView*) setupAnnotation {
    MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"MyCustomAnnotation"];
    pinView.animatesDrop = YES;
    pinView.canShowCallout = YES;
    switch ([self.logoID intValue]) {
        case 0:
            pinView.pinTintColor = [UIColor grayColor];
            break;
        case 1:
            pinView.pinTintColor = [UIColor purpleColor];
            break;
        case 2:
            pinView.pinTintColor = [UIColor brownColor];
            break;
        case 3:
            pinView.pinTintColor = [UIColor orangeColor];
            break;
        case 4:
            pinView.pinTintColor = [UIColor redColor];
            break;
        case 5:
            pinView.pinTintColor = [UIColor blueColor];
            break;
        default:
            break;
    }
    pinView.centerOffset = CGPointMake(10, -20);
    NSString *nameImage = [NSString stringWithFormat:@"PartyLogo_Small_%@",self.logoID];
    UIImageView *myCustomImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    myCustomImage.image = [UIImage imageNamed:nameImage];
    myCustomImage.contentMode = UIViewContentModeScaleAspectFit;
    pinView .leftCalloutAccessoryView = myCustomImage;
    return pinView;
}


- (CLLocationCoordinate2D) useLocationString:(NSString*)loc {
    
    CLLocationCoordinate2D location;
    NSArray * locationArray = [loc componentsSeparatedByString: @";"];
    
    if (locationArray.count > 1) {
        location.latitude = [[locationArray objectAtIndex:0] doubleValue];
        location.longitude = [[locationArray objectAtIndex:1] doubleValue];
    } else {
        location.latitude = -3000;
        location.longitude = -3000;
    }
    
    return location;
    
}


@end
