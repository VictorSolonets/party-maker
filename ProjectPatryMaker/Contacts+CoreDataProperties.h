//
//  Contacts+CoreDataProperties.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/18/16.
//  Copyright © 2016 Softheme. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contacts.h"

NS_ASSUME_NONNULL_BEGIN

@interface Contacts (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *comment;
@property (nonatomic) double timeStart;
@property (nonatomic) int64_t creatorID;
@property (nonatomic) double timeEnd;
@property (nonatomic) float longitude;
@property (nonatomic) float latitude;
@property (nullable, nonatomic, retain) NSManagedObject *relationship;

@end

NS_ASSUME_NONNULL_END
