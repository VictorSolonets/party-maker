//
//  PPMDataStore.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/10/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMDataStore.h"
#import "PPMPartyTableViewController.h"
#import "PPMEntityUser+CoreDataProperties.h"
#import "PPMEntityParty.h"
#import "_PPMEntityParty.h"

#import <CoreData/CoreData.h>

@interface PPMDataStore()

@property (readwrite, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readwrite, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (readwrite, strong, nonatomic) NSManagedObjectContext *mainObjectContext;
@property (readwrite, strong, nonatomic) NSManagedObjectContext *backgroungObjectContext;

@end

@implementation PPMDataStore

#pragma mark - init singletone

+ (instancetype) sharedData {
    static PPMDataStore *sharedData = nil;
    @synchronized(self) {
        if(sharedData == nil) {
            sharedData = [self new];
        }
    }
    return sharedData;
}

#pragma mark - methods for implement Core Stack


- (void) addPartyWithName:(NSString*)name
                   LogoID:(NSNumber*)logoID
                 Latitude:(NSString*)latitude
                Longitude:(NSString*)longitude
                  Comment:(NSString*)comment
                TimeStart:(NSNumber*)timeStart
                  TimeEnd:(NSNumber*)timeEnd
                  PartyID:(NSNumber*)partyID
                CreatorID:(NSNumber*)creatorID
               completion:(void(^)())completion{
    
    NSManagedObjectContext *context = self.backgroungObjectContext;
    
    [context performBlock:^{
        
        PPMEntityParty *party = nil;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        request.entity = [NSEntityDescription entityForName:NSStringFromClass([PPMEntityParty class]) inManagedObjectContext:context];
        
        request.predicate = [NSPredicate predicateWithFormat:@"partyID = %@", partyID];
        NSError *executeFetchError = nil;
        party = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
        
        if (executeFetchError) {
            NSLog(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [partyID intValue], [executeFetchError localizedDescription]);
        }
        if (!party) {
            party = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PPMEntityParty class])inManagedObjectContext:context];
        }
        party.name = name;
        party.logoID = logoID;
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        if ([latitude isKindOfClass:[NSNull class]]) {
            party.latitude = @"0.0";
            party.longitude = @"0.0";
        } else {
            party.latitude = latitude;
            party.longitude = longitude;
        }
        party.comment = comment;
        party.timeEnd = timeEnd;
        party.timeStart = timeStart;
        party.creatorID = creatorID;
        party.partyID = partyID;
        NSError *error = nil;
        if ([context save:&error] == NO) {
            NSLog( @"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }];
}


- (void) addUser:(NSString *)login
          userID:(NSNumber *)ID
           email:(NSString *)email
      completion:(void (^)())completion {
    
    NSManagedObjectContext *context = self.backgroungObjectContext;
    
    [context performBlock:^{
        
        
        PPMEntityUser *user = nil;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        request.entity = [NSEntityDescription entityForName:NSStringFromClass([PPMEntityUser class]) inManagedObjectContext:context];
        request.predicate = [NSPredicate predicateWithFormat:@"userID = %@", ID];
        NSError *executeFetchError = nil;
        user = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
        
        
        NSNumber *userID = ID;
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.userID = ID;
        
        if (executeFetchError) {
            NSLog(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [ID intValue], [executeFetchError localizedDescription]);
            
        }
        if (!user) {
            
            user = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PPMEntityUser class]) inManagedObjectContext:[self backgroungObjectContext]];
            user.name = login;
            user.userID = ID;
            user.email = email;
            
            NSError *error = nil;
            if ([[self backgroungObjectContext] save:&error] == NO) {
                NSAssert(NO, @"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion();
                }
            });
        }
        
    }];
}

- (void) deleteAllParty {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([PPMEntityParty class])];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    NSManagedObjectContext *context = self.backgroungObjectContext;
    [context performBlock:^{
        NSError *deleteError = nil;
        if ([self.persistentStoreCoordinator executeRequest:delete withContext:context error:&deleteError] == NO) {
            NSAssert(NO, @"Error saving context: %@\n%@", [deleteError localizedDescription], [deleteError userInfo]);
        }
    }];
}

- (void) deleteAllUser {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([PPMEntityUser class])];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    NSManagedObjectContext *context = self.backgroungObjectContext;
    [context performBlock:^{
        NSError *deleteError = nil;
        if ([self.persistentStoreCoordinator executeRequest:delete withContext:context error:&deleteError] == NO) {
            NSAssert(NO, @"Error saving context: %@\n%@", [deleteError localizedDescription], [deleteError userInfo]);
        }
    }];
    
}

- (void)deletePartyWithID:(NSNumber*)partyID
               completion:(void (^)())completion {
    NSManagedObjectContext *context = self.backgroungObjectContext;
    [context performBlock:^{
        PPMEntityParty *party = [PPMEntityParty fetchWithPartyID:partyID context:context];
        [self.backgroungObjectContext deleteObject:party];
        NSError *error;
        if (![self.backgroungObjectContext save:&error]) {
            NSLog(@"Error - %@ in method  %s", error, __PRETTY_FUNCTION__);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }];
}

- (PPMEntityParty*) fetchObjectFromEntity:(NSString *)entityName
                                   forKey:(NSString *)key
                                withValue:(NSNumber *)value {
    NSManagedObjectContext *context = self.mainObjectContext;
    NSFetchRequest *fetch = [NSFetchRequest new];
    fetch.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    fetch.predicate = [NSPredicate predicateWithFormat:@"%K == %@", key, value];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetch error:&error];
    if ( error ) {
        NSLog(@"Fetching failed with error %@", error);
    }
    return [fetchedObjects firstObject];
}

#pragma mark - Perform block

- (void) performBlock:(void (^)(NSError *completionError))block
            withError:(NSError *)error{
    if (block) {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(error);
        });
    }
}

#pragma mark - Core Data stack

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ProjectModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DataBased.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)mainObjectContext {
    if (_mainObjectContext != nil) {
        return _mainObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _mainObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType: NSMainQueueConcurrencyType];
    [_mainObjectContext setPersistentStoreCoordinator:coordinator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applyMainContextChanges:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:self.mainObjectContext];
    
    return _mainObjectContext;
}

- (NSManagedObjectContext *)backgroungObjectContext {
    if (_backgroungObjectContext != nil) {
        return _backgroungObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _backgroungObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType: NSPrivateQueueConcurrencyType];
    [_backgroungObjectContext setPersistentStoreCoordinator:coordinator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applyBackgroundContextChanges:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:self.backgroungObjectContext];
    
    return _backgroungObjectContext;
}

#pragma mark - method for Notification

- (void)applyBackgroundContextChanges:(NSNotification *)notification {
    @synchronized (self) {
        [[self mainObjectContext] performBlock:^{
            [[self mainObjectContext] mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}

- (void)applyMainContextChanges:(NSNotification *)notification {
    @synchronized (self) {
        [[self backgroungObjectContext] performBlock:^{
            [[self backgroungObjectContext] mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}

@end

