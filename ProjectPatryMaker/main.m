//
//  main.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Softheme. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPMAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PPMAppDelegate class]));
    }
}
