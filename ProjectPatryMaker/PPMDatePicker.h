//
//  PPMDatePicker.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/9/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PPMDatePicker;
@protocol PPMDatePickerDelegate <NSObject>
@required
- (void) clickDoneDatePicker: (PPMDatePicker*) datePickerView;
- (void) clickCancelDatePicker: (PPMDatePicker*) datePickerView;
@end
@interface PPMDatePicker : UIView
@property (nonatomic, weak)  id <PPMDatePickerDelegate> delegate;
@property (nonatomic, weak)  IBOutlet UIDatePicker* datePicker;

-(IBAction)onDoneDate:(id)sender;
-(IBAction)onCancelDate:(id)sender;
@end
