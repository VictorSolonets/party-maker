//
//  PPMDatePickerView.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/9/16.
//  Copyright © 2016 Softheme. All rights reserved.
//

#import "PPMDatePickerView.h"

@interface PPMDatePickerView ()

@end

@implementation PPMDatePickerView

- (IBAction)dismissMe:(id)sender {
    [self.delegate performSelector:@selector(dismissMe:) withObject:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
