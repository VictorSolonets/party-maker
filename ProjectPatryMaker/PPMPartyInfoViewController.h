//
//  PPMPartyInfoViewController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/12/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPMEntityParty.h"

@interface PPMPartyInfoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *partyName;
@property (strong, nonatomic) IBOutlet UILabel *partyDescription;
@property (strong, nonatomic) IBOutlet UILabel *partyDate;
@property (strong, nonatomic) IBOutlet UILabel *partyStart;
@property (strong, nonatomic) IBOutlet UILabel *partyEnd;
@property (strong, nonatomic) IBOutlet UIImageView *partyImage;

@property (nonatomic) NSNumber *timeStart;
@property (nonatomic) NSNumber *timeEnd;

@property (nonatomic) NSNumber *currentPicture;
@property (nonatomic) NSNumber *partyID;
@property (nonatomic) NSNumber *creatorID;

@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *latitude;

@property (nonatomic) NSNumber *indexParty;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewTo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintsView;

@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

- (void) setParty : (PPMEntityParty*) party Index : (NSNumber*) indexParty;

@property (nonatomic) PPMEntityParty* party;

@end
