// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PPMEntityUser.h instead.

@import CoreData;

extern const struct PPMEntityUserAttributes {
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *userID;
} PPMEntityUserAttributes;

extern const struct PPMEntityUserRelationships {
	__unsafe_unretained NSString *parties;
} PPMEntityUserRelationships;

@class PPMEntityParty;

@interface PPMEntityUserID : NSManagedObjectID {}
@end

@interface _PPMEntityUser : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PPMEntityUserID* objectID;

@property (nonatomic, strong) NSString* email;

//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* userID;

@property (atomic) int64_t userIDValue;
- (int64_t)userIDValue;
- (void)setUserIDValue:(int64_t)value_;

//- (BOOL)validateUserID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *parties;

- (NSMutableSet*)partiesSet;

@end

@interface _PPMEntityUser (PartiesCoreDataGeneratedAccessors)
- (void)addParties:(NSSet*)value_;
- (void)removeParties:(NSSet*)value_;
- (void)addPartiesObject:(PPMEntityParty*)value_;
- (void)removePartiesObject:(PPMEntityParty*)value_;

@end

@interface _PPMEntityUser (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveUserID;
- (void)setPrimitiveUserID:(NSNumber*)value;

- (int64_t)primitiveUserIDValue;
- (void)setPrimitiveUserIDValue:(int64_t)value_;

- (NSMutableSet*)primitiveParties;
- (void)setPrimitiveParties:(NSMutableSet*)value;

@end
