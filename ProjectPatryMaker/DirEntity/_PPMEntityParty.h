// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PPMEntityParty.h instead.

@import CoreData;

extern const struct PPMEntityPartyAttributes {
	__unsafe_unretained NSString *comment;
	__unsafe_unretained NSString *creatorID;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *logoID;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *partyID;
	__unsafe_unretained NSString *timeEnd;
	__unsafe_unretained NSString *timeStart;
} PPMEntityPartyAttributes;

extern const struct PPMEntityPartyRelationships {
	__unsafe_unretained NSString *creator;
} PPMEntityPartyRelationships;

@class PPMEntityUser;

@interface PPMEntityPartyID : NSManagedObjectID {}
@end

@interface _PPMEntityParty : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PPMEntityPartyID* objectID;

@property (nonatomic, strong) NSString* comment;

//- (BOOL)validateComment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* creatorID;

@property (atomic) int64_t creatorIDValue;
- (int64_t)creatorIDValue;
- (void)setCreatorIDValue:(int64_t)value_;

//- (BOOL)validateCreatorID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* latitude;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* logoID;

@property (atomic) int64_t logoIDValue;
- (int64_t)logoIDValue;
- (void)setLogoIDValue:(int64_t)value_;

//- (BOOL)validateLogoID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* longitude;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* partyID;

@property (atomic) int64_t partyIDValue;
- (int64_t)partyIDValue;
- (void)setPartyIDValue:(int64_t)value_;

//- (BOOL)validatePartyID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* timeEnd;

@property (atomic) int64_t timeEndValue;
- (int64_t)timeEndValue;
- (void)setTimeEndValue:(int64_t)value_;

//- (BOOL)validateTimeEnd:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* timeStart;

@property (atomic) int64_t timeStartValue;
- (int64_t)timeStartValue;
- (void)setTimeStartValue:(int64_t)value_;

//- (BOOL)validateTimeStart:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) PPMEntityUser *creator;

//- (BOOL)validateCreator:(id*)value_ error:(NSError**)error_;

@end

@interface _PPMEntityParty (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveComment;
- (void)setPrimitiveComment:(NSString*)value;

- (NSNumber*)primitiveCreatorID;
- (void)setPrimitiveCreatorID:(NSNumber*)value;

- (int64_t)primitiveCreatorIDValue;
- (void)setPrimitiveCreatorIDValue:(int64_t)value_;

- (NSString*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSString*)value;

- (NSNumber*)primitiveLogoID;
- (void)setPrimitiveLogoID:(NSNumber*)value;

- (int64_t)primitiveLogoIDValue;
- (void)setPrimitiveLogoIDValue:(int64_t)value_;

- (NSString*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePartyID;
- (void)setPrimitivePartyID:(NSNumber*)value;

- (int64_t)primitivePartyIDValue;
- (void)setPrimitivePartyIDValue:(int64_t)value_;

- (NSNumber*)primitiveTimeEnd;
- (void)setPrimitiveTimeEnd:(NSNumber*)value;

- (int64_t)primitiveTimeEndValue;
- (void)setPrimitiveTimeEndValue:(int64_t)value_;

- (NSNumber*)primitiveTimeStart;
- (void)setPrimitiveTimeStart:(NSNumber*)value;

- (int64_t)primitiveTimeStartValue;
- (void)setPrimitiveTimeStartValue:(int64_t)value_;

- (PPMEntityUser*)primitiveCreator;
- (void)setPrimitiveCreator:(PPMEntityUser*)value;

@end
