// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PPMEntityParty.m instead.

#import "_PPMEntityParty.h"

const struct PPMEntityPartyAttributes PPMEntityPartyAttributes = {
	.comment = @"comment",
	.creatorID = @"creatorID",
	.latitude = @"latitude",
	.logoID = @"logoID",
	.longitude = @"longitude",
	.name = @"name",
	.partyID = @"partyID",
	.timeEnd = @"timeEnd",
	.timeStart = @"timeStart",
};

const struct PPMEntityPartyRelationships PPMEntityPartyRelationships = {
	.creator = @"creator",
};

@implementation PPMEntityPartyID
@end

@implementation _PPMEntityParty

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PPMEntityParty" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PPMEntityParty";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PPMEntityParty" inManagedObjectContext:moc_];
}

- (PPMEntityPartyID*)objectID {
	return (PPMEntityPartyID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"creatorIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"creatorID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logoIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logoID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"partyIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"partyID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"timeEndValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"timeEnd"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"timeStartValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"timeStart"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic comment;

@dynamic creatorID;

- (int64_t)creatorIDValue {
	NSNumber *result = [self creatorID];
	return [result longLongValue];
}

- (void)setCreatorIDValue:(int64_t)value_ {
	[self setCreatorID:@(value_)];
}

- (int64_t)primitiveCreatorIDValue {
	NSNumber *result = [self primitiveCreatorID];
	return [result longLongValue];
}

- (void)setPrimitiveCreatorIDValue:(int64_t)value_ {
	[self setPrimitiveCreatorID:@(value_)];
}

@dynamic latitude;

@dynamic logoID;

- (int64_t)logoIDValue {
	NSNumber *result = [self logoID];
	return [result longLongValue];
}

- (void)setLogoIDValue:(int64_t)value_ {
	[self setLogoID:@(value_)];
}

- (int64_t)primitiveLogoIDValue {
	NSNumber *result = [self primitiveLogoID];
	return [result longLongValue];
}

- (void)setPrimitiveLogoIDValue:(int64_t)value_ {
	[self setPrimitiveLogoID:@(value_)];
}

@dynamic longitude;

@dynamic name;

@dynamic partyID;

- (int64_t)partyIDValue {
	NSNumber *result = [self partyID];
	return [result longLongValue];
}

- (void)setPartyIDValue:(int64_t)value_ {
	[self setPartyID:@(value_)];
}

- (int64_t)primitivePartyIDValue {
	NSNumber *result = [self primitivePartyID];
	return [result longLongValue];
}

- (void)setPrimitivePartyIDValue:(int64_t)value_ {
	[self setPrimitivePartyID:@(value_)];
}

@dynamic timeEnd;

- (int64_t)timeEndValue {
	NSNumber *result = [self timeEnd];
	return [result longLongValue];
}

- (void)setTimeEndValue:(int64_t)value_ {
	[self setTimeEnd:@(value_)];
}

- (int64_t)primitiveTimeEndValue {
	NSNumber *result = [self primitiveTimeEnd];
	return [result longLongValue];
}

- (void)setPrimitiveTimeEndValue:(int64_t)value_ {
	[self setPrimitiveTimeEnd:@(value_)];
}

@dynamic timeStart;

- (int64_t)timeStartValue {
	NSNumber *result = [self timeStart];
	return [result longLongValue];
}

- (void)setTimeStartValue:(int64_t)value_ {
	[self setTimeStart:@(value_)];
}

- (int64_t)primitiveTimeStartValue {
	NSNumber *result = [self primitiveTimeStart];
	return [result longLongValue];
}

- (void)setPrimitiveTimeStartValue:(int64_t)value_ {
	[self setPrimitiveTimeStart:@(value_)];
}

@dynamic creator;

@end

