//
//  PPMPartyTableViewController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyTableViewController.h"
#import "PPMEntityParty.h"
#import "PPMPartyMakeViewController.h"
#import "PPMPartyTableViewCell.h"
#import "PPMPartyInfoViewController.h"
#import "PPMPartyMakerSDK.h"
#import "PPMEntityUser.h"
#import "PPMDataStore.h"
#import "PPMPartyNotification.h"
#import "PPMEntityUser.h"
#import "NSString+ParseDate.h"

@interface PPMPartyTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) PPMPartyTableViewController *tableViewController;

@end

@implementation PPMPartyTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayOfParties = [NSMutableArray new];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadFromServer) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadFromServer];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayOfParties.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PPMEntityParty *party = [self.arrayOfParties objectAtIndex:indexPath.row];
    [PPMPartyNotification addNotification:party];
    PPMPartyTableViewCell *cell = (PPMPartyTableViewCell*) [tableView dequeueReusableCellWithIdentifier:[PPMPartyTableViewCell reuseIdentifier] forIndexPath:indexPath];
    [cell configureWithName:party.name Date:[NSString parseDate:party.timeStart] Image:party.logoID Time:[NSString timeStart:party.timeStart] Adress:party.latitude];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue
                sender:(PPMPartyTableViewController*)sender {
    if ([segue.identifier isEqualToString:@"PartyCellReuseIdentifier"]) {
        PPMPartyInfoViewController *viewSeque = [segue destinationViewController];
        [viewSeque.view layoutIfNeeded];
        NSIndexPath *index = [self.partyTableView indexPathForSelectedRow];
        PPMEntityParty *party = [self.arrayOfParties objectAtIndex: index.row];
        [viewSeque setParty : party Index : 0];
    }
}

- (void) loadFromServer {
    __weak PPMPartyTableViewController *weakSelf = self;
     if ([[PPMPartyMakerSDK sharedManager] getNetworkStatus]) {
    [[PPMPartyMakerSDK sharedManager] getAllPartyFromUser:[[PPMDataStore sharedData] userID]
                                                 Callback:^(NSDictionary *response, NSError *error) {
                                                     [weakSelf parseParty:response];
                                                     [weakSelf.partyTableView reloadData];

                                                 }];
     }
     else {
         [self addActionView:^(UIAlertAction * action) {} WithTitle:@"Error!" Message:NSLocalizedStringFromTable(@"Internet is missing!", @"Language",nil)];
         [weakSelf.refreshControl endRefreshing];
     }
}

- (void) parseParty:(NSDictionary*) response {

   self.arrayOfParties = [NSMutableArray new];
   [PPMPartyNotification removeNotifications];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    __weak PPMPartyTableViewController *weakSelf = self;
    if (![[[response objectForKey:@"response"] class] isSubclassOfClass: [NSNull class]]) {
        for (NSDictionary *allParties in [response objectForKey:@"response"]) {
            [[PPMDataStore sharedData] addPartyWithName:[allParties objectForKey:@"name"]
                                                 LogoID:[f numberFromString:[allParties objectForKey:@"logo_id"]]
                                               Latitude:[allParties objectForKey:@"latitude"]
                                              Longitude:[allParties objectForKey:@"longitude"]
                                                Comment:[allParties objectForKey:@"comment"]
                                              TimeStart:[f numberFromString:[allParties objectForKey:@"start_time"]]
                                                TimeEnd:[f numberFromString:[allParties objectForKey:@"end_time"]]
                                                PartyID:[f numberFromString:[allParties objectForKey:@"id"]]
                                              CreatorID:[f numberFromString:[allParties objectForKey:@"creator_id"]]
                                             completion:^{
                                                 [weakSelf.arrayOfParties addObject:[PPMEntityParty fetchWithPartyID:[allParties objectForKey:@"id"] context:[[PPMDataStore sharedData] mainObjectContext]]];
                                                 [weakSelf.partyTableView reloadData];
                                            }];
        }
        [self.refreshControl endRefreshing];
    }
}


- (void)addActionView:(void (^)())actionBlock WithTitle:(NSString*)newTitle
              Message:(NSString*)newMessage{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
