//
//  PPMEntityParty+CoreDataProperties.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PPMEntityParty+CoreDataProperties.h"

@implementation PPMEntityParty (CoreDataProperties)

@dynamic name;
@dynamic timeStart;
@dynamic timeEnd;
@dynamic longitude;
@dynamic comment;
@dynamic latitude;
@dynamic creatorID;
@dynamic partyID;
@dynamic logoID;
@dynamic creator;

@end
