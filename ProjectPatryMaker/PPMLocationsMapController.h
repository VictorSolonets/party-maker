//
//  PPMLocationsMapController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/24/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface PPMLocationsMapController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectFriendsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *resetToDefaulf;

@end
