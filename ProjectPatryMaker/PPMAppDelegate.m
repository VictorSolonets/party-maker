//
//  PPMAppDelegate.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMAppDelegate.h"
#import "PPMPartyTableViewController.h"
#import "PPMPartyMakerSDK.h"
#import "PPMEntityParty.h"
#import "PPMDataStore.h"

@interface PPMAppDelegate ()
@end

@implementation PPMAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainViewController" bundle:nil];
    NSInteger userID = [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue];
    
    NSLog(@"%li",userID);
    if (!userID) {
        UIViewController *rootVC = [storyboard instantiateInitialViewController];
        self.window.rootViewController = rootVC;
        [self.window makeKeyAndVisible];

    } else {
        [PPMDataStore sharedData].userID = [NSNumber numberWithInteger:userID];
        UITabBarController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"PPMTabController"];
        self.window.rootViewController = tabBar;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}


@end
