//
//  NSString+ParseDate.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "NSString+ParseDate.h"
#import "_PPMEntityParty.h"

@implementation NSString (ParseDate)

+ (NSString*) parseDate : (NSNumber*) partyDate {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[partyDate doubleValue]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+ (NSString*) timeStart : (NSNumber*) partyStart {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[partyStart doubleValue]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+ (NSString*) timeEnd : (NSNumber*) partyEnd {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[partyEnd doubleValue]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+ (NSInteger) getStartTimeMinute : (PPMEntityParty*) party {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [dateFormatter dateFromString:[NSString timeStart:[party valueForKey:PPMEntityPartyAttributes.timeStart]]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    return (minute+hour*60);
}

+ (NSInteger) getEndTimeMinute : (PPMEntityParty*) party {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [dateFormatter dateFromString:[NSString timeEnd:[party valueForKey:PPMEntityPartyAttributes.timeEnd]]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    return (minute+hour*60);
}

@end
