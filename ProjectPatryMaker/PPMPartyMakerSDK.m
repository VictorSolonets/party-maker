//
//  PPMPartyMakerSDK.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/16/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyMakerSDK.h"
#import "PPMEntityParty.h"
#import "PPMDataStore.h"


@interface PPMPartyMakerSDK ()
@property (nonatomic, strong) NSURLSession *defaultSession;
@property (nonatomic) NSString *linkURLAPI;

@property (nonatomic) Reachability *networkReachability;
@property (nonatomic) NetworkStatus networkStatus;

@end

@implementation PPMPartyMakerSDK


#pragma mark inits

+ (instancetype)sharedManager {
    static PPMPartyMakerSDK *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        instance = [[PPMPartyMakerSDK alloc] init];
        if(instance) {
            [instance configureSessionAndAPI];
        }
    });
    return instance;
}

#pragma mark - status network


- (BOOL) getNetworkStatus {
    @synchronized(self.networkReachability) {
        if (!self.networkReachability) {
            self.networkReachability = [Reachability reachabilityForInternetConnection];
        }
    }
    self.networkStatus = [self.networkReachability currentReachabilityStatus];
    return (self.networkStatus != NotReachable);
}

#pragma mark - configure session

- (void) configureSessionAndAPI {
    self.linkURLAPI = @"http://itworksinua.km.ua/party/";
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 50;
    sessionConfig.timeoutIntervalForResource = 50;
    sessionConfig.allowsCellularAccess = NO;
    self.defaultSession = [NSURLSession sessionWithConfiguration:sessionConfig];
}

- (NSMutableURLRequest*) createRequestWithType:(NSString*) methodType
                                     MethodAPI:(NSString*) methodAPI
                                        Header:(NSDictionary*) header
                                    Parameters:(NSDictionary*) parameters {
    
    NSString *path = [NSString stringWithFormat:@"%@%@",self.linkURLAPI,methodAPI];
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
    [request setHTTPMethod:methodType];
    
    if ([methodType isEqualToString:@"POST"]) {
        NSString *str = @"";
        for (NSString *key in [parameters allKeys])
            str = [str stringByAppendingString: [NSString stringWithFormat:@"%@=%@&", key, [parameters objectForKey:key]]];

        NSData *requestData = [str dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:requestData];
        
    } else if ([methodType isEqualToString:@"GET"]) {
        NSString *str = @"?";
        for (NSString *key in [parameters allKeys])
            str = [str stringByAppendingString: [NSString stringWithFormat:@"%@=%@&", key, [parameters objectForKey:key]]];

        path = [path stringByAppendingString:str];
        path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request.URL = [NSURL URLWithString:path];
    }
    
    request.URL = [NSURL URLWithString:path];
    return request;
}

- (void) loginWithUser:(NSString*) username
           andpassword:(NSString*) password
              callback:(void (^) (NSDictionary *response, NSError *error))block {
    NSURLRequest *request = [self createRequestWithType:@"GET"
                                              MethodAPI:@"login"
                                                 Header:nil
                                             Parameters:@{@"name":username, @"password":password}];
    
    [[self.defaultSession dataTaskWithRequest:request
                            completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
                            [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

#pragma mark - manipulated methods

- (void) deletePartyWithID:(NSNumber*)partyID
                 CreatorID:(NSNumber*)creatorID
                  Callback:(void (^) (NSDictionary *response, NSError *error))block {
    
    NSURLRequest *request = [self createRequestWithType:@"GET"
                                              MethodAPI:@"deleteParty"
                                                 Header:nil
                                             Parameters:@{@"party_id": partyID,@"creator_id":creatorID}];
    
    [[self.defaultSession dataTaskWithRequest:request
                            completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
                            [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

- (void) registerEmail:(NSString*)email
                  Name:(NSString*)username
              Password:(NSString*)password
              Callback:(void (^) (NSDictionary* response, NSError *error))block {
    
    if (!username) {
        username = @"";
    }
    
    NSURLRequest *request = [self createRequestWithType:@"POST"
                                              MethodAPI:@"register"
                                                 Header:nil
                                             Parameters:@{@"email":email,
                                                          @"password":password,
                                                          @"name":username}];
    
    [[self.defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data,
                                                                          NSURLResponse * _Nullable response,
                                                                          NSError * _Nullable error) {
    NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
    [self ppm_callBlockOnMainThread:block responce:responce error:error];}] resume];
}

- (void) addPartyWithName:(NSString*)name
                   LogoID:(NSNumber*)logoID
                 Latitude:(NSString*)latitude
                Longitude:(NSString*)longitude
                  Comment:(NSString*)comment
                TimeStart:(long) timeStart
                  TimeEnd:(long) timeEnd
                CreatorID:(NSNumber*)creatorID
                   UserID:(NSNumber*)partyID
                 Callback:(void (^)(NSDictionary* response, NSError *error))block {
    
    NSURLRequest *request;
    if (!partyID) {
        request = [self createRequestWithType:@"POST" MethodAPI:@"addParty" Header:nil Parameters:
                   @{@"name":name,
                     @"start_time":[NSNumber numberWithLong : timeStart],
                     @"end_time":[NSNumber numberWithLong : timeEnd],
                     @"logo_id":logoID,
                     @"comment":comment,
                     @"creator_id" : creatorID,
                     @"latitude" : latitude ,
                     @"longitude" : longitude
                     }];
    } else {
        request = [self createRequestWithType:@"POST" MethodAPI:@"addParty" Header:nil Parameters:
                   @{@"party_id":partyID,
                     @"name":name,
                     @"start_time":[NSNumber numberWithLong : timeStart],
                     @"end_time":[NSNumber numberWithLong : timeEnd],
                     @"logo_id":logoID,
                     @"comment":comment,
                     @"creator_id" : creatorID,
                     @"latitude" : latitude ,
                     @"longitude" : longitude
                     }];
    }
    
    [[PPMDataStore sharedData] addPartyWithName:name
                    LogoID:logoID
                    Latitude:latitude
                    Longitude:longitude
                    Comment:comment
                    TimeStart:[NSNumber numberWithLong : timeStart]
                    TimeEnd:[NSNumber numberWithLong : timeEnd]
                    PartyID:partyID
                    CreatorID:creatorID
                    completion:^{}];
    
    [[self.defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
        [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

- (void) getAllPartyFromUser:(NSNumber*)createrID
                    Callback:(void (^)(NSDictionary* response, NSError *error))block {
    NSURLRequest *request = [self createRequestWithType:@"GET" MethodAPI:@"party" Header:nil Parameters:@{@"creator_id" : createrID}];
    [[self.defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
        [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

- (void) getAllUsersCallback:(void (^)(NSDictionary* response, NSError *error))block {
    NSURLRequest *request = [self createRequestWithType:@"GET"
                                              MethodAPI:@"allUsers"
                                                 Header:nil
                                             Parameters:nil];
    [[self.defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
        [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

- (void) deleteUser:(NSNumber*)creator_id
           callback:(void (^)(NSDictionary* response, NSError *error))block {
    NSURLRequest *req = [self createRequestWithType:@"GET"
                                          MethodAPI:@"deleteUser"
                                             Header:nil
                                         Parameters:@{@"creator_id":creator_id}];
    [[self.defaultSession dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *responce = [self serialize:data statusCode:(NSNumber *)[response valueForKey:@"statusCode"]];
        [self ppm_callBlockOnMainThread:block responce:responce error:error];
    }] resume];
}

#pragma mark - serialize data
- (NSDictionary* )serialize:(NSData*)data
                 statusCode:(NSNumber*)statusCode {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if (statusCode)
        [dict setValue:statusCode forKey:@"statusCode"];
    else
        [dict setValue:@505 forKey:@"statusCode"];
    id jsonArray;
    if (data) jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (!jsonArray) jsonArray = [NSNull null];
    [dict setValue:jsonArray forKey:@"response"];
    return dict;
}

#pragma mark - help methods

- (void)ppm_callBlockOnMainThread:(void(^)(NSDictionary* response, NSError *error))block
                         responce:(NSDictionary*)responceDict error:(NSError*)error {
    
    dispatch_async( dispatch_get_main_queue(), ^{
        if (block) {
            block(responceDict, error);
        }
    });
}

@end
