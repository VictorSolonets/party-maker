//
//  PPMEntityUser.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMEntityUser.h"
#import "PPMDataStore.h"

@implementation PPMEntityUser

+ (NSArray*) getAllUsers {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([PPMEntityUser class])inManagedObjectContext:[[PPMDataStore sharedData] mainObjectContext]]];
    NSError *error = nil;
    NSArray *items = [[[PPMDataStore sharedData] mainObjectContext] executeFetchRequest:request error:&error];
    return items;
}

@end
