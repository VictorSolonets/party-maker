//
//  PPMPartyNotification.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/26/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPMEntityParty.h"

@interface PPMPartyNotification : UILocalNotification

+ (void)addNotification:(PPMEntityParty *)party;
+ (void)addNotifications:(NSArray *)parties;
+ (void)removeNotifications;

@end
