//
//  PPMPartyTableViewController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPMPartyTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *partyTableView;
@property (nonatomic) NSMutableArray *arrayOfParties;

@end

