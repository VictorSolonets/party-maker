//
//  PPMPartyMakerSDK.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/16/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPMEntityUser.h"
#import "PPMEntityParty.h"
#import "Reachability.h"

@interface PPMPartyMakerSDK : NSObject

+ (instancetype)sharedManager;

- (BOOL) getNetworkStatus;

- (void) loginWithUser:(NSString*)_username
           andpassword:(NSString*)_password
              callback:(void (^) (NSDictionary *response, NSError *error))block ;

- (void) registerEmail:(NSString*)email
                  Name:(NSString*)username
              Password:(NSString*)password
              Callback:(void (^)(NSDictionary* response, NSError *error))block;

- (void) deleteUser:(NSNumber*)creator_id
           callback:(void (^)(NSDictionary* response, NSError *error))block;

- (void) addPartyWithName:(NSString*)name
                   LogoID:(NSNumber*)logoID
                 Latitude:(NSString*)latitude
                Longitude:(NSString*)longitude
                  Comment:(NSString*)comment
                TimeStart:(long)timeStart
                  TimeEnd:(long)timeEnd
                CreatorID:(NSNumber*)creatorID
                   UserID:(NSNumber*) partyID
                 Callback:(void (^)(NSDictionary* response, NSError *error))block ;

- (void) getAllPartyFromUser:(NSNumber*)createrID
                    Callback:(void (^)(NSDictionary* response, NSError *error))block;

- (void) deletePartyWithID:(NSNumber*)partyID
                CreatorID:(NSNumber*)creatorID
                  Callback:(void (^)(NSDictionary *response, NSError *error))block;

- (void) getAllUsersCallback:(void (^)(NSDictionary* response, NSError *error))block ;

@end
