//
//  PPMMakePartyController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Softheme. All rights reserved.
//

#import "PPMMakePartyController.h"
#import "PPMParty.h"

@interface PPMMakePartyController () <UITextFieldDelegate, UIAlertViewDelegate, UITextViewDelegate, UIScrollViewDelegate, UIPageViewControllerDelegate>
@property (nonatomic) UILabel *labelMin, *labelMax;
@property (nonatomic) UIView *ovalBack;
@property (nonatomic) UITextField *patryNameField;
@property (nonatomic) UISlider *minTimeForParty, *maxTimeForParty;
@property (nonatomic) UIButton *dateChange, *cancelButton, *saveButton;
@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) UIToolbar *toolbarDatePicker;
@property (nonatomic) UIPageControl *pageControl;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UITextView *descriptionParty;
@property (nonatomic) NSString *textViewText;
@end
@implementation PPMMakePartyController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
    self.title = @"CREATE PARTY";
    
    /* Configure right line */
    
    self.ovalBack = [[UIView alloc] initWithFrame:CGRectMake(8, 20, 13, 13)];
    self.ovalBack.backgroundColor = [UIColor grayColor];
    self.ovalBack.layer.cornerRadius = 6.5;
    [self.view addSubview:self.ovalBack];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(14, 25, 1, 451)];
    lineView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:lineView];
    
    /* Configure right ovals */
    
    [self addOval:86];
    [self addOval:135];
    [self addOval:182];
    [self addOval:224];
    [self addOval:300];
    [self addOval:419];
    [self addOval:537];
    
    
    /* Configure Labels Names */
    
    [self addLabels:82 andText:@"CHOOSE DAY"];
    [self addLabels:131 andText:@"PARTY NAME"];
    [self addLabels:178 andText:@"START"];
    [self addLabels:220 andText:@"END"];
    [self addLabels:296 andText:@"LOGO"];
    [self addLabels:415 andText:@"DESCRIPTION"];
    [self addLabels:533 andText:@"FINAL"];
    
    /* Configure Date Change Button */
    
    self.dateChange = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dateChange.frame = (CGRect){120, 7, 190, 36};
    [self.dateChange setTitle:@"CHOOSE DATE" forState:UIControlStateNormal];
    [self.dateChange setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.dateChange.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:16];
    [self.dateChange addTarget:self action:@selector(onDateChanged) forControlEvents:UIControlEventTouchUpInside];
    [self.dateChange setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.dateChange.backgroundColor = [UIColor colorWithRed:239./255 green:177./255 blue:27./255 alpha:1.];
    self.dateChange.layer.cornerRadius = 5;
    [self.view addSubview:self.dateChange];
    
    /*Configure Text Field */
    
    self.patryNameField = [[UITextField alloc] initWithFrame:(CGRect){120, 56, 190, 40}];
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Your party name" attributes: @{NSForegroundColorAttributeName : [UIColor colorWithRed:44./255 green:48./255 blue:55./255 alpha:1.]}];
    self.patryNameField.attributedPlaceholder = str;
    self.patryNameField.textColor = [UIColor whiteColor];
    self.patryNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.patryNameField.textAlignment = NSTextAlignmentCenter;
    self.patryNameField.font = [UIFont fontWithName:@"MyriadPro-Regular" size:16];
    self.patryNameField.backgroundColor = [UIColor colorWithRed:35./255 green:37./255 blue:43./255 alpha:1.];
    self.patryNameField.layer.cornerRadius = 5;
    [self.patryNameField setReturnKeyType:UIReturnKeyDone];
    self.patryNameField.delegate = self;
    [self.view addSubview:self.patryNameField];
    
    /* Configure min Slider range */
    
    self.minTimeForParty = [[UISlider alloc] initWithFrame:(CGRect){120, 107, 190, 31}];
    self.minTimeForParty.minimumValueImage = [UIImage imageNamed:@"TimePopup"];
    self.minTimeForParty.value = 0.0;
    self.minTimeForParty.minimumTrackTintColor = [UIColor colorWithRed:239./255 green:177./255 blue:27./255 alpha:1.];
    self.minTimeForParty.maximumTrackTintColor = [UIColor colorWithRed:28./255 green:30./255 blue:35./255 alpha:1.];
    [self.minTimeForParty addTarget:self action:@selector(onSlideMin) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.minTimeForParty];
    
    /* Configure min label for range */
    
    self.labelMin = [[UILabel alloc] initWithFrame: (CGRect){123, 118, 30, 13}];
    self.labelMin.text = @"00:00";
    self.labelMin.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
    self.labelMin.textColor = [UIColor whiteColor];
    [self.view addSubview:self.labelMin];
    
    /* Configure max Slider range */
    
    self.maxTimeForParty = [[UISlider alloc] initWithFrame:(CGRect){117, 146, 190, 31}];
    self.maxTimeForParty.maximumValueImage = [UIImage imageNamed:@"TimePopup-2"];
    self.maxTimeForParty.minimumTrackTintColor = [UIColor colorWithRed:239./255 green:177./255 blue:27./255 alpha:1.];
    self.maxTimeForParty.maximumTrackTintColor = [UIColor colorWithRed:28./255 green:30./255 blue:35./255 alpha:1.];
    [self.maxTimeForParty addTarget:self action:@selector(onSlideMax) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.maxTimeForParty];
    
    /* Configure max label for range */
    
    self.labelMax = [[UILabel alloc] initWithFrame: (CGRect){278, 155, 30, 13}];
    self.labelMax.text = @"00:30";
    self.labelMax.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
    self.labelMax.textColor = [UIColor whiteColor];
    [self.view addSubview:self.labelMax];
    
    /* Configure Page Control */
    
    [self addPageControl];
    
    /* Configure text view */
    
    UIView *lineUnderText = [[UIView alloc] initWithFrame:CGRectMake(120, 302, 190, 10)];
    lineUnderText.backgroundColor = [UIColor colorWithRed:48./255 green:156./255 blue:205./255 alpha:1.];
    lineUnderText.layer.cornerRadius = 3;
    lineUnderText.layer.masksToBounds = YES;
    [self.view addSubview:lineUnderText];
    
    self.descriptionParty = [[UITextView alloc] initWithFrame:(CGRect){120, 307, 190, 90}];
    self.descriptionParty.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
    self.descriptionParty.backgroundColor = [UIColor colorWithRed:35./255 green:37./255 blue:43./255 alpha:1.];
    self.descriptionParty.textColor = [UIColor whiteColor];
    self.descriptionParty.delegate = self;
    [self.view addSubview:self.descriptionParty];
    
    /* Confugure toolbar for description */
    
    UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 64, 375, 50)];
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *itemCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                   style:UIBarButtonItemStylePlain target:self action:@selector(onCancelKeyboard)];
    UIBarButtonItem *itemDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStyleDone target:self action:@selector(onDoneKeyboard)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:nil action:nil];
    itemDone.tintColor = itemCancel.tintColor = [UIColor whiteColor];
    toolbar.items = @[itemCancel, flexibleSpace, itemDone];
    [toolbar sizeToFit];
    self.descriptionParty.inputAccessoryView = toolbar;
    
    /* Configure save button */
    
    self.saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.saveButton.frame = (CGRect){120, 411, 190, 36};
    [self.saveButton setTitle:@"SAVE" forState:UIControlStateNormal];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:16];
    [self.saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    [self.saveButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.saveButton.backgroundColor = [UIColor colorWithRed:140./255 green:186./255 blue:29./255 alpha:1.];
    self.saveButton.layer.cornerRadius = 5;
    [self.view addSubview:self.saveButton];
    
    
    /* Configure cancel button */
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.frame = (CGRect){120, 460, 190, 36};
    [self.cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:16];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.cancelButton.backgroundColor = [UIColor colorWithRed:236./255 green:71./255 blue:19./255 alpha:1.];
    self.cancelButton.layer.cornerRadius = 5;
    [self.view addSubview:self.cancelButton];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    __block __weak PPMMakePartyController *weakSelf = self;
    self.maxTimeForParty.enabled = self.scrollView.scrollEnabled = self.pageControl.enabled = self.saveButton.enabled = NO;
    self.textViewText = textView.text;
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect viewFrame = weakSelf.view.frame;
                         viewFrame = CGRectMake(0, -131, 320, 460);
                         weakSelf.view.frame = viewFrame;
                     } completion:^(BOOL finished) {
                     }];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView; {
    [self moveBackOvel : 417];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    __block __weak PPMMakePartyController *weakSelf = self;
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect viewFrame = weakSelf.view.frame;
                         viewFrame = CGRectMake(0 ,64, 320, 567);
                         weakSelf.view.frame = viewFrame;
                     } completion:^(BOOL finished) {
               
                     }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self switchEnabledOnFieldClick:NO];
    [self moveBackOvel : 133];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self switchEnabledOnFieldClick:YES];
    [self.patryNameField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.patryNameField resignFirstResponder];
    return YES;
}

- (void) onDateChanged {
    
    self.dateChange.enabled = self.minTimeForParty.enabled = self.maxTimeForParty.enabled = self.patryNameField.enabled = self.scrollView.scrollEnabled = NO;
    
    [self moveBackOvel : 84];
    
    /* Configure Date picker */
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:(CGRect){0, 570-64, 375, 200}];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.datePicker];
    
    /* Configure Toolbar for Date picker */
    
    self.toolbarDatePicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 570-64, 320, 50)];
    self.toolbarDatePicker.barStyle = UIBarStyleBlackTranslucent;
    self.toolbarDatePicker.backgroundColor = [UIColor colorWithRed:68./255 green:73./255 blue:83./255 alpha:1.];
    UIBarButtonItem *itemCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onCancel)];
    UIBarButtonItem *itemDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDone)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    itemDone.tintColor = itemCancel.tintColor = [UIColor whiteColor];
    self.toolbarDatePicker.items = @[itemCancel, flexibleSpace, itemDone];
    [self.view addSubview:self.toolbarDatePicker];
    __block __weak PPMMakePartyController *weakSelf = self;
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.datePicker.frame = (CGRect){0, 368-64, 375, 200};
                         weakSelf.toolbarDatePicker.frame = (CGRect){0, 320-64, 320, 50};
                     }
                     completion:^(BOOL finished){
                     }];
}


- (void) doneChangeDate {
    
    __block __weak PPMMakePartyController *weakSelf = self;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.datePicker.frame = (CGRect){0, 567, 375, 200};
                     }
                     completion:^(BOOL finished){
                     }];
    
}

- (void) onSlideMin {
    [self moveBackOvel : 180];
    
    int time = self.minTimeForParty.value * 1409;
    self.labelMin.text = [NSString stringWithFormat:@"%02i:%02i", time/60, time - (time/60)*60];
    if (self.minTimeForParty.value > self.maxTimeForParty.value) {
        self.labelMax.text = [NSString stringWithFormat:@"%02i:%02i", (time+30)/60, time + 30 - ((time+30)/60)*60 ];
        self.maxTimeForParty.value = (self.minTimeForParty.value);
    }
}

- (void) onSlideMax {
    
    [self moveBackOvel : 222];
    
    int time = self.maxTimeForParty.value * 1409;
    
    self.labelMax.text = [NSString stringWithFormat:@"%02i:%02i", (time+30)/60, time + 30 - ((time+30)/60)*60 ];
    
    if (self.minTimeForParty.value > self.maxTimeForParty.value) {
        self.labelMin.text = [NSString stringWithFormat:@"%02i:%02i", time/60, time - (time/60)*60];
        self.minTimeForParty.value = (self.maxTimeForParty.value);
    }
}

- (void) cancelClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) saveClick {
    [self moveBackOvel : 535];
    NSString *error = [NSString new];
    if ([self.dateChange.titleLabel.text isEqual:@"CHOOSE DATE"])
        error = @"You should choose a date!";
    else if ([self.patryNameField.text isEqualToString:@""])
        error = @"You should enter a party name!";
    else if ([self.descriptionParty.text isEqualToString:@""])
        error = @"You should enter a party description!";
    else {
        //PPMParty *party = [[PPMParty alloc] initWithDate:[self.dateChange.titleLabel text] Name:[self.patryNameField text] TimeStart:[self.labelMin text] TimeEnd:[self.labelMax text] Image:[NSString stringWithFormat:@"PartyLogo_Small_%li",self.pageControl.currentPage] Description:[self.descriptionParty text]];
        //[party saveParty];
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error!" message:error preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style: UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion: nil];
}
- (void) switchEnabledOnFieldClick : (BOOL) ON {
    self.dateChange.enabled = self.minTimeForParty.enabled = self.maxTimeForParty.enabled = self.patryNameField.enabled = self.scrollView.scrollEnabled = ON;
}
- (void)onCancel {
    __block __weak PPMMakePartyController *weakSelf = self;
    [self switchEnabledOnFieldClick:YES];
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.datePicker.frame = (CGRect){0, 570, 375, 200};
                         weakSelf.toolbarDatePicker.frame = (CGRect){0, 570, 320, 50};
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void)onDone {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM.YY"];
    NSString *str = [dateFormatter stringFromDate:[self.datePicker date]];
    [self.dateChange setTitle:str forState:UIControlStateNormal];
    __block __weak PPMMakePartyController *weakSelf = self;
    [self switchEnabledOnFieldClick:YES];
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.datePicker.frame = (CGRect){0, 570, 375, 200};
                         weakSelf.toolbarDatePicker.frame = (CGRect){0, 570, 320, 50};
                     }
                     completion:^(BOOL finished){
                         [weakSelf.datePicker removeFromSuperview];
                     }];
    
}

- (void)onDoneKeyboard {
    self.textViewText = self.descriptionParty.text;
    self.maxTimeForParty.enabled = self.scrollView.scrollEnabled = self.pageControl.enabled = self.saveButton.enabled = YES;
    [self.descriptionParty resignFirstResponder];
}

- (void)onCancelKeyboard {
    self.descriptionParty.text = self.textViewText;
    self.maxTimeForParty.enabled = self.scrollView.scrollEnabled = self.pageControl.enabled = self.saveButton.enabled = YES;
    [self.descriptionParty resignFirstResponder];
}

- (void)scrollViewDidEndDecelerating: (UIScrollView *)scrollView {
    [self moveBackOvel:298];
    NSInteger currentPage = scrollView.contentOffset.x/190;
    [self.pageControl setCurrentPage:currentPage];
}

- (void)onPageChanged:(UIPageControl *)sender {
    [self moveBackOvel:298];
    CGPoint contentOffset = CGPointMake(sender.currentPage * self.scrollView.frame.size.width, 0);
    [self.scrollView setContentOffset:contentOffset animated:YES];
}

- (void) moveBackOvel : (int) xValue {
    __block __weak PPMMakePartyController *weakSelf = self;
    [UIView animateWithDuration:0.05
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         weakSelf.ovalBack.frame = (CGRect){8, xValue-64, 13, 13};
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void) addOval : (int) yValue {
    UIView *oval = [[UIView alloc] initWithFrame:CGRectMake(10, yValue-64, 9, 9)];
    oval.backgroundColor = [UIColor whiteColor];
    oval.layer.cornerRadius = 4.5;
    [self.view addSubview:oval];
}

- (void) addLabels : (int) yValue andText : (NSString*) text {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(28, yValue-64, 92, 17) ];
    label.text = text;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
    [self.view addSubview:label];}

- (void) addPageControl {
    UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(120, 255-64, 190, 85)];
    pageScrollView.backgroundColor = [UIColor colorWithRed:68/255. green:73/255. blue:83/255. alpha:1];
    pageScrollView.layer.cornerRadius = 5;
    [pageScrollView setShowsHorizontalScrollIndicator:NO];
    pageScrollView.delegate = self;
    pageScrollView.pagingEnabled = YES;
    
    for (int i = 0; i < 6 ; i++) {
        self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"PartyLogo_Small_%i", i]]];
        float xOffset = (pageScrollView.frame.size.width / 2 - self.imageView.frame.size.width / 2) + i * 190;
        self.imageView.frame =CGRectMake(xOffset, 9.5, self.imageView.frame.size.width, self.imageView.frame.size.height);
        [pageScrollView addSubview:self.imageView];
    }
    
    pageScrollView.contentSize = CGSizeMake(190 * 6, 63);
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(120, 334-64, 190, 17)];
    self.pageControl.numberOfPages = 6;
    self.pageControl.currentPage = 0;
    self.pageControl.layer.cornerRadius = 5;
    self.pageControl.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.pageControl.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:29/255. green:30/255. blue:36/255. alpha:1];
    self.pageControl.backgroundColor = [UIColor colorWithRed:68/255. green:73/255. blue:83/255. alpha:1];
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    [self.pageControl setUserInteractionEnabled:YES];
    
    [self.pageControl addTarget:self
                    action:@selector(onPageChanged:)
          forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:pageScrollView];
    [self.view addSubview:self.pageControl];
    
//    self.pageControl = pageControl;
    self.scrollView = pageScrollView;
}

@end
