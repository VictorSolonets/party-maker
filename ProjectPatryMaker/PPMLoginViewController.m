//  PPMLoginViewController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/15/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMLoginViewController.h"
#import "PPMPartyTableViewController.h"
#import "PPMPartyMakerSDK.h"
#import "PPMEntityParty.h"
#import "PPMDataStore.h"
#import "PPMAPIController.h"

@interface PPMLoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewWithLogin;
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;
@end

@implementation PPMLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSNumber *userID = nil;
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"userID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.login setReturnKeyType:UIReturnKeyNext];
    [self.password setReturnKeyType:UIReturnKeyContinue];
    self.login.delegate = self.password.delegate = self;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.viewWithLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewWithLogin.layer.borderWidth = 1.0f;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Protocol Text Field

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(textField.tag == 1) {
        [self.password becomeFirstResponder];
    }
    if (textField.tag == 2) {
        [self.login resignFirstResponder];
    }
    
}

- (IBAction)loginButton:(id)sender {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    [[PPMPartyMakerSDK sharedManager] loginWithUser:self.login.text andpassword:self.password.text callback:^(NSDictionary *response, NSError *error) {
        if (![[[response objectForKey:@"response"] class] isSubclassOfClass: [NSNull class]]) {
            if ([[[response valueForKey:@"response"] objectForKey:@"name"] isEqualToString:self.login.text]) {
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainViewController" bundle:nil];
                UITabBarController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"PPMTabController"];
                
                [[PPMDataStore sharedData] addUser:[[response valueForKey:@"response"] objectForKey:@"name"] userID:[f numberFromString:[[response valueForKey:@"response"] objectForKey:@"id"]] email:[[response valueForKey:@"response"] objectForKey:@"email"]completion:^{
                    
                } ];
                
                
                [self presentViewController:tabBar animated:YES completion:nil];
            } else {
                [self addActionView:^(UIAlertAction * action) {
                } WithTitle:NSLocalizedStringFromTable([[response valueForKey:@"response"] objectForKey:@"status"], @"Language",nil)
                            Message:NSLocalizedStringFromTable([[response valueForKey:@"response"] objectForKey:@"msg"], @"Language",nil)];
            }
        } else {
            [self addActionView:^(UIAlertAction * action) {
            } WithTitle:NSLocalizedStringFromTable(@"Error!", @"Language",nil) Message:NSLocalizedStringFromTable(@"Internet is missing!", @"Language",nil)];
        }
    }];
}

- (IBAction)registerButton:(id)sender {
    
    [[PPMPartyMakerSDK sharedManager] registerEmail:[NSString stringWithFormat:@"%@@gmail.com",self.login.text] Name:self.login.text Password:self.password.text Callback:^(NSDictionary *response, NSError *error) {
        if (![[[response objectForKey:@"response"] class] isSubclassOfClass: [NSNull class]]) {
            [self addActionView:^(UIAlertAction * action) {
            } WithTitle:NSLocalizedStringFromTable([[response valueForKey:@"response"] objectForKey:@"status"], @"Language",nil)
                        Message:NSLocalizedStringFromTable([[response valueForKey:@"response"] objectForKey:@"msg"], @"Language",nil)];
        } else {
            [self addActionView:^(UIAlertAction * action) {
            } WithTitle:NSLocalizedStringFromTable(@"Error!", @"Language",nil) Message:NSLocalizedStringFromTable(@"Internet is missing!", @"Language",nil)];
        }
    }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)addActionView:(void (^)())actionBlock WithTitle : (NSString*) newTitle Message : (NSString*) newMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
    }];
}

@end
