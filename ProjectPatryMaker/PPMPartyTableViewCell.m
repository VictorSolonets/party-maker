//
//  PPMPartyTableViewCell.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/12/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyTableViewCell.h"

@interface PPMPartyTableViewCell ()

@end

@implementation PPMPartyTableViewCell

- (void)configureWithName:(NSString*)newPartyName
                     Date:(NSString*)newPartyDate
                    Image:(NSNumber*)avatar
                     Time:(NSString*)newPartyTime
                   Adress:(NSString*)newAddress {
    
    self.partyImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"PartyLogo_Small_%@",avatar]];
    self.partyName.text = newPartyName;
    self.partyDate.text = newPartyDate;
    self.partyTime.text = newPartyTime;
    self.locationAdress.text = newAddress;
}

- (void)prepareForReuse {
    
    [super prepareForReuse];
    
    self.partyImage.image = nil;
    self.partyName.text = nil;
    self.partyDate.text = nil;
    self.partyTime.text = nil;
    self.locationAdress.text = nil;
}

+ (NSString*)reuseIdentifier {
    
    return @"PartyCellReuseIdentifier";
}

@end
