//
//  PPMEntityParty+CoreDataProperties.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PPMEntityParty.h"

NS_ASSUME_NONNULL_BEGIN

@interface PPMEntityParty (CoreDataProperties)

@property (nullable, nonatomic) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *timeStart;
@property (nullable, nonatomic, retain) NSNumber *timeEnd;
@property (nullable, nonatomic, retain) NSString *longitude;
@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSNumber *creatorID;
@property (nullable, nonatomic, retain) NSNumber *partyID;
@property (nullable, nonatomic, retain) NSNumber *logoID;
@property (nullable, nonatomic, retain) PPMEntityUser *creator;

@end

NS_ASSUME_NONNULL_END
