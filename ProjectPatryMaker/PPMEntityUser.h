//
//  PPMEntityUser.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PPMEntityUser : NSManagedObject

+ (NSArray*) getAllUsers;


@end

NS_ASSUME_NONNULL_END

#import "PPMEntityUser+CoreDataProperties.h"
