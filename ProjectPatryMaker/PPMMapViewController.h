//
//  PPMMapViewController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/23/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class MVPPickLocationVC;
@protocol PPMSubtitleCoordinateDelegate <NSObject>

@required
- (void) sendSubtitle:(NSString*)name
             location:(NSString*)location;
@end

@interface PPMMapViewController : UIViewController <CLLocationManagerDelegate>


@property (weak, nonatomic) id <PPMSubtitleCoordinateDelegate> delegate;

@property (strong, nonatomic) CLLocationManager *manager;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) UILongPressGestureRecognizer *press;

@property (nonatomic) NSString *partyName;
@property (nonatomic) NSNumber *logoID;
@property (nonatomic) NSString *location;
@property (nonatomic) NSString *locationName;

@end
