//
//  PPMEntityUser+CoreDataProperties.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PPMEntityUser+CoreDataProperties.h"

@implementation PPMEntityUser (CoreDataProperties)

@dynamic userID;
@dynamic name;
@dynamic email;
@dynamic parties;

@end
