//
//  PPMLocationsMapController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/24/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMLocationsMapController.h"
#import "PPMDataStore.h"
#import "PPMAnnotationParty.h"
#import "PPMPartyMakerSDK.h"
#import "PPMPartyInfoViewController.h"
#import "PPMAPIController.h"
#import "_PPMEntityParty.h"

@interface PPMLocationsMapController() <MKMapViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate>

@property (nonatomic) NSMutableArray *annotationArray;
@property (nonatomic) NSMutableArray *arrayUsers;
@property (nonatomic) NSMutableArray *arrayParties;
@property (nonatomic) NSMutableArray *arrayNameUser;

@property (nonatomic) MKPinAnnotationView *pinView;

@property (nonatomic) CLLocationManager *locationManager;

@property (nonatomic) UIPickerView *userPickerView;
@property (nonatomic) UIToolbar *userToolbar;

@end
@implementation PPMLocationsMapController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
    self.annotationArray = [NSMutableArray new];
    self.arrayUsers = [NSMutableArray new];
    self.arrayParties = [NSMutableArray new];
    self.arrayNameUser = [NSMutableArray new];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    
    [self addPinsForUserID:[[PPMDataStore sharedData] userID]];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[PPMPartyMakerSDK sharedManager] getNetworkStatus])
        [self getAllUsers];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied){
        [self addActionView:^(UIAlertAction * action) {
            NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:settingsURL];
        } WithTitle:NSLocalizedStringFromTable(@"Location services are off", @"Language",nil) Message:NSLocalizedStringFromTable(@"To use background location you must turn on 'When in use' in the Location Services Settings", @"Language",nil)];
    }
}


#pragma mark ActionView

- (void)addActionView:(void (^)())actionBlock WithTitle : (NSString*) newTitle Message : (NSString*) newMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Go to Settings", @"Language",nil)
                                                          style:UIAlertActionStyleDefault handler:actionBlock];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"NO", @"Language",nil)  style: UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [self presentViewController:alert animated:YES completion: nil];
}


#pragma mark - additing methods

- (IBAction)resetPins :(id)sender {
    [self addPinsForUserID:[[PPMDataStore sharedData] userID]];
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.arrayUsers.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@", [self.arrayNameUser objectAtIndex:row]];
}


#pragma mark methods get users

- (void) getAllUsers {
    [[PPMPartyMakerSDK sharedManager] getAllUsersCallback:^(NSDictionary *response, NSError *error) {
        dispatch_async( dispatch_get_main_queue(), ^{
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            for (NSDictionary *users in [response objectForKey:@"response"]) {
                [self.arrayUsers addObject:[f numberFromString:[users objectForKey:@"id"]]];
                [self.arrayNameUser addObject:[users objectForKey:@"name"]];
                [[PPMAPIController sharedInstance] getAllPartiesFromUserID:[self.arrayUsers lastObject]];
            }
        });
    }];
    
}

#pragma mark methods add pins

- (void) addPinsForUserID:(NSNumber*)userID {
    [self.mapView removeAnnotations:self.annotationArray];
    [self.annotationArray removeAllObjects];
    for (PPMEntityParty *party in [PPMEntityParty getAllPatiesFromUserID:userID]) {
        PPMAnnotationParty * annotation = [[PPMAnnotationParty alloc]
                                           initWithParty:party];
        if (annotation.coordinate.latitude != -3000.0)
            [self.annotationArray addObject:annotation];
    }
    [self.mapView showAnnotations:self.annotationArray animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else {
        PPMAnnotationParty *customAnnotation = (PPMAnnotationParty*) annotation;
        self.pinView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"MyCustomAnnotation"];
        self.pinView = [customAnnotation setupAnnotation];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        rightButton.tintColor = [UIColor blueColor];
        self.pinView.rightCalloutAccessoryView = rightButton;
        return self.pinView;
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    PPMAnnotationParty *customAnnotation = (PPMAnnotationParty*) view.annotation;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainViewController" bundle:nil];
    PPMPartyInfoViewController* infoController = [storyboard instantiateViewControllerWithIdentifier:@"PartyInfoController"];
    [infoController setParty:[customAnnotation party] Index:@-1];
    [self.navigationController pushViewController:infoController animated:YES];
}

#pragma mark add Picker View to View


- (IBAction)selectFriends :(id)sender {
    self.userPickerView = [UIPickerView new];
    self.userPickerView.delegate = self;
    self.userPickerView.dataSource = self;
    self.userPickerView.backgroundColor = [UIColor whiteColor];
    self.userPickerView.frame = (CGRect){0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height/2} ;
    self.userToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 44)];
    self.userToolbar.translucent = YES;
    [self.userToolbar sizeToFit];
    self.userToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Done", @"Language",nil)
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(clickDoneUserPicker:)];
    UIBarButtonItem *spacerButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Cancel", @"Language",nil)
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(clickCancelUserPicker:)];
    
    doneButton.tintColor = cancelButton.tintColor = [UIColor blackColor];
    [self.userToolbar setItems:@[cancelButton,spacerButton,doneButton]];
    self.userToolbar.backgroundColor = [UIColor whiteColor];
    self.userToolbar.userInteractionEnabled = YES;
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame = self.userToolbar.frame;
        frame.origin.y -= self.userPickerView.frame.size.height;
        self.userToolbar.frame = frame;
        frame = self.userPickerView.frame;
        frame.origin.y -= self.userPickerView.frame.size.height;
        self.userPickerView.frame = frame;
    }];
    [self.view addSubview:self.userPickerView];
    [self.view addSubview:self.userToolbar];
    self.selectFriendsButton.enabled = self.resetToDefaulf.enabled = NO;
}

#pragma mark - methods for user picker

- (void) clickCancelUserPicker: (UIPickerView*) datePicker{
    [UIView
     animateWithDuration: 0.4 animations:^{
         CGRect frame = self.userPickerView.frame;
         frame.origin.y = self.view.frame.size.height;
         self.userPickerView.frame = frame;
         self.userToolbar.frame = frame;
     } completion:^(BOOL finished) {
         [self.userPickerView removeFromSuperview];
         [self.userToolbar removeFromSuperview];
     }];
   self.selectFriendsButton.enabled = self.resetToDefaulf.enabled = YES;
}

- (void) clickDoneUserPicker: (UIPickerView*) datePicker{
    NSInteger selectedID = [self.userPickerView selectedRowInComponent:0] % self.arrayUsers.count;
    NSNumber *userID = [self.arrayUsers objectAtIndex:selectedID];
    [self addPinsForUserID:userID];
    
    [UIView animateWithDuration: 0.4 animations:^{
        CGRect frame = self.userPickerView.frame;
        frame.origin.y = self.view.frame.size.height;
        self.userPickerView.frame = frame;
        self.userToolbar.frame = frame;
    } completion:^(BOOL finished) {
        [self.userPickerView removeFromSuperview];
        [self.userToolbar removeFromSuperview];
    }];
    self.selectFriendsButton.enabled = self.resetToDefaulf.enabled = YES;
}


@end
