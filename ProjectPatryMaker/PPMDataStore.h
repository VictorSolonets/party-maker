//
//  PPMDataStore.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/10/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPMEntityParty.h"
#import <CoreData/CoreData.h>

@interface PPMDataStore : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *mainObjectContext;
@property (readwrite, strong) NSNumber* userID;

+ (instancetype) sharedData;

- (void) addPartyWithName:(NSString*)name
                   LogoID:(NSNumber*)logoID
                 Latitude:(NSString*)latitude
                Longitude:(NSString*)longitude
                  Comment:(NSString*) comment
                TimeStart:(NSNumber*) timeStart
                  TimeEnd:(NSNumber*) timeEnd
                  PartyID:(NSNumber*)creatorID
                CreatorID:(NSNumber*) partyID
               completion:(void (^)())completion;

- (void)deletePartyWithID:(NSNumber*)partyID
               completion:(void (^)())completion;

- (void) deleteAllParty;

- (void) deleteAllUser;

- (PPMEntityParty*) fetchObjectFromEntity:(NSString *)entityName
                                   forKey:(NSString *)key
                                withValue:(NSNumber *)value;

- (void) addUser:(NSString *)login
          userID:(NSNumber *)ID
           email:(NSString *)email
      completion:(void (^)())completion;


@end
