//
//  PPMAppDelegate.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/3/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

