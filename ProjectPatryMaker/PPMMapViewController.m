//
//  PPMMapViewController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/23/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMMapViewController.h"
#import "PPMAnnotationParty.h"
#import "PPMPartyMakeViewController.h"
@interface PPMMapViewController () <MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic) MKPinAnnotationView *pinView;

@end

@implementation PPMMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
    self.press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnMap:)];
    self.press.minimumPressDuration = 0.3f;
    self.press.delegate = self;
    
    [self.mapView addGestureRecognizer:self.press];
    
    if ([self.partyName isEqualToString : @"" ]) {
        self.partyName = @"New Party";
    }
    
    if (self.location && ![self.location isEqualToString:@""]) {
        CLLocationCoordinate2D locationPin;
        locationPin = [self useLocationString:self.location];
        PPMAnnotationParty * annotation = [[PPMAnnotationParty alloc] initWithCoordinate:locationPin Title:self.partyName subtitle:self.locationName logoID:self.logoID];
        [self.mapView addAnnotation:annotation];
    }
    [self.manager requestWhenInUseAuthorization];
}

- (void) viewDidAppear:(BOOL)animated {
    self.manager = [[CLLocationManager alloc] init];
    self.manager.delegate = self;
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied){
        [self addActionView:^(UIAlertAction * action) {
            NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:settingsURL];
        } WithTitle:NSLocalizedStringFromTable(@"Location services are off", @"Language",nil) Message:NSLocalizedStringFromTable(@"To use background location you must turn on 'When in use' in the Location Services Settings", @"Language",nil)];
    }
}


#pragma mark ActionView

- (void)addActionView:(void (^)())actionBlock WithTitle : (NSString*) newTitle Message : (NSString*) newMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Go to Settings", @"Language",nil)
                                                          style:UIAlertActionStyleDefault handler:actionBlock];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"NO", @"Language",nil)  style: UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [self presentViewController:alert animated:YES completion: nil];
}


-(void) touchOnMap:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan ) {
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        CGPoint point = [recognizer locationInView:self.mapView];
        CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:tapPoint.latitude longitude:tapPoint.longitude];
        __weak PPMMapViewController *wSelf = self;
        [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            PPMAnnotationParty * annotation = [[PPMAnnotationParty alloc] initWithCoordinate:tapPoint
                                                                                       Title:wSelf.partyName
                                                                                    subtitle:[[placemarks lastObject]name]
                                                                                      logoID:wSelf.logoID];
            [wSelf.mapView addAnnotation:annotation];
        }];
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else {
        if (self.pinView) {
            return self.pinView;
        } else {
            PPMAnnotationParty *customAnnotation = (PPMAnnotationParty*) annotation;
            self.pinView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"MyCustomAnnotation"];
            self.pinView = [customAnnotation setupAnnotation];
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
            rightButton.tintColor = [UIColor blackColor];
            [rightButton addTarget:self action:@selector(setLocation) forControlEvents:UIControlEventTouchUpInside];
            self.pinView .rightCalloutAccessoryView = rightButton;
            return self.pinView;
        }
    }
}

- (CLLocationCoordinate2D) useLocationString:(NSString*)loc {

    CLLocationCoordinate2D location;
    NSArray * locationArray = [loc componentsSeparatedByString: @";"];

    if (locationArray.count > 1) {
        location.latitude = [[locationArray objectAtIndex:0] doubleValue];
        location.longitude = [[locationArray objectAtIndex:1] doubleValue];
    }
    return location;
}

-(void)setLocation {
    CLLocation *location = [[CLLocation alloc]initWithLatitude:self.pinView.annotation.coordinate.latitude longitude:self.pinView.annotation.coordinate.longitude];
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendSubtitle:location:)]) {
        [self.delegate performSelector:@selector(sendSubtitle:location:) withObject:self.pinView.annotation.subtitle withObject:[NSString stringWithFormat:@"%f;%f",location.coordinate.latitude,location.coordinate.longitude]];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma  mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKCoordinateRegion theRegion = self.mapView.region;
    if (self.location) {
        if (![self.location isEqualToString:@""]) {
            CLLocationCoordinate2D locationPin = [self useLocationString:self.location];
            theRegion.center = locationPin;
        }
    } else {
        theRegion.center = userLocation.location.coordinate;
    }
    theRegion.span.longitudeDelta /=100;
    theRegion.span.latitudeDelta /=100;
    [self.mapView setRegion:theRegion animated:YES];
}


@end
