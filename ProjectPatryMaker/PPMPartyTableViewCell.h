//
//  PPMPartyTableViewCell.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/12/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPMPartyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *partyImage;
@property (weak, nonatomic) IBOutlet UILabel *partyName;
@property (weak, nonatomic) IBOutlet UILabel *partyDate;
@property (weak, nonatomic) IBOutlet UILabel *partyTime;
@property (weak, nonatomic) IBOutlet UILabel *locationAdress;

- (void)configureWithName:(NSString*)newPartyName
                     Date:(NSString*)newPartyDate
                    Image:(NSNumber*)avatar
                     Time:(NSString*)newPartyTime
                   Adress:(NSString*)newAddress;

- (void)prepareForReuse;

+ (NSString*)reuseIdentifier;

@end
