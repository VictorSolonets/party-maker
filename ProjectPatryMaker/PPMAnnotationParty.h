//
//  PPMAnnotationParty.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/23/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "PPMEntityParty.h"

@interface PPMAnnotationParty : NSObject <MKAnnotation>

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (nonatomic) NSNumber *logoID;

@property (nonatomic) PPMEntityParty *party;

- (instancetype)  initWithCoordinate:(CLLocationCoordinate2D)location
                               Title:(NSString*)title
                            subtitle:(NSString*)subtitle
                              logoID:(NSNumber*)logoID;

- (instancetype)  initWithParty : (PPMEntityParty*) party;

- (MKPinAnnotationView*) setupAnnotation;

@end
