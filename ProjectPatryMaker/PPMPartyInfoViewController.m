//
//  PPMPartyInfoViewController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/12/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMPartyInfoViewController.h"
#import "PPMPartyMakeViewController.h"
#import "PPMEntityParty.h"
#import "PPMPartyMakerSDK.h"
#import "PPMEntityUser.h"
#import "PPMDataStore.h"
#import "NSString+ParseDate.h"

@implementation PPMPartyInfoViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.party) {
        self.partyName.text = [self.party valueForKey:PPMEntityPartyAttributes.name];
        self.partyDescription.text = [self.party valueForKey:PPMEntityPartyAttributes.comment];
        self.partyID = [self.party valueForKey:PPMEntityPartyAttributes.partyID];
        self.currentPicture =[self.party valueForKey:PPMEntityPartyAttributes.logoID];
        self.partyImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"PartyLogo_Small_%@",self.currentPicture]];
        self.creatorID = [self.party valueForKey:PPMEntityPartyAttributes.creatorID];
        self.latitude = [self.party valueForKey:PPMEntityPartyAttributes.latitude];
        self.longitude = [self.party valueForKey:PPMEntityPartyAttributes.longitude];
        self.timeStart = [self.party valueForKey:PPMEntityPartyAttributes.timeStart];
        self.timeEnd = [self.party valueForKey:PPMEntityPartyAttributes.timeEnd];
        self.partyDate.text = [NSString parseDate: self.timeStart];
        self.partyStart.text = [NSString timeStart: self.timeStart];
        self.partyEnd.text = [NSString timeEnd: self.timeEnd];
        
        if ([self.indexParty isEqualToNumber:[NSNumber numberWithInt:-1]]) {
            [self.locationButton removeFromSuperview];
            [self.editButton removeFromSuperview];
            [self.deleteButton removeFromSuperview];
            self.constraintViewTo.constant -= self.locationButton.frame.size.height*2;
            self.constraintsView.constant -= self.locationButton.frame.size.height*3;
        } else {
            self.indexParty = self.indexParty;
        }
        self.partyImage.layer.borderColor = [UIColor whiteColor].CGColor;
        self.partyImage.layer.borderWidth = 1.0f;
        
        if([self.latitude isEqualToString:@""]) {
            self.locationButton.enabled = NO;
        } else {
            self.locationButton.enabled = YES;
        }

    }
}

- (void) setParty : (PPMEntityParty*) party Index : (NSNumber*) indexParty {
    self.party = party;
    self.indexParty = indexParty;
}

- (IBAction)getLocation:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainViewController" bundle:nil];
    PPMMapViewController* mapController = [ storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    mapController.location = self.longitude;
    mapController.locationName = self.latitude;
    mapController.partyName = self.partyName.text;
    mapController.logoID = self.currentPicture;
    mapController.press = nil;
    [self.navigationController pushViewController:mapController animated:YES];
}


- (IBAction)deletePartyButton:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"DELETE PARTY",@"Language",nil)
                                                                   message:NSLocalizedStringFromTable(@"Do you realy want to delete this party?", @"Language",nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __weak PPMPartyInfoViewController *weakSelf = self;
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"YES",@"Language",nil)
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [[PPMPartyMakerSDK sharedManager] deletePartyWithID:weakSelf.partyID CreatorID:[[PPMDataStore sharedData] userID ] Callback:^(NSDictionary *response, NSError *error){
                                                                      if ([[[response objectForKey:@"response"] class] isSubclassOfClass: [NSNull class]]) {
                                                                          [weakSelf addActionView:^(UIAlertAction * action) {
                                                                          } WithTitle:NSLocalizedStringFromTable(@"Error!", @"Language",nil) Message:NSLocalizedStringFromTable(@"Internet is missing!", @"Language",nil)];
                                                                      } else {
                                                                          [[PPMDataStore sharedData] deletePartyWithID:weakSelf.partyID completion:^{
                                                                              [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                                                          }];
                                                                      }
                                                              }];
                                                          }];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"NO",@"Language",nil) style: UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    }

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SegueForEditParty"]) {
        
        PPMPartyMakeViewController *viewSeque = [segue destinationViewController];
        
        [viewSeque setParty : self.partyID];
    }
}

- (void)addActionView:(void (^)())actionBlock WithTitle : (NSString*) newTitle Message : (NSString*) newMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:newTitle
                                                                   message:newMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
    }];
}

@end
