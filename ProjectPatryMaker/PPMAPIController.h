//
//  PPMAPIController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/22/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PPMDataStore.h"
#import "PPMPartyMakerSDK.h"

@interface PPMAPIController : UIViewController

+ (instancetype)sharedInstance;

- (void) getAllPartiesFromUserID:(NSNumber*)userID;

@end
