//
//  PPMEntityParty.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/19/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMEntityParty.h"
#import "PPMEntityUser.h"
#import "PPMDataStore.h"


@implementation PPMEntityParty

+ (PPMEntityParty*)fetchWithPartyID:(NSNumber*)partyID
                            context:(NSManagedObjectContext*)context {
    
    PPMEntityParty *party = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    request.entity = [NSEntityDescription entityForName:NSStringFromClass([PPMEntityParty class]) inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"partyID = %@", partyID];
    NSError *executeFetchError = nil;
    party = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        NSLog(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd),
              [partyID intValue], [executeFetchError localizedDescription]);
    }
    
    return party;
}

+ (NSArray*) getAllPatiesFromUserID:(NSNumber*)userID{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([PPMEntityParty class]) inManagedObjectContext:[[PPMDataStore sharedData] mainObjectContext]];
    fetchRequest.returnsDistinctResults = NO;
    [fetchRequest setResultType:NSDictionaryResultType];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"creatorID=%@", userID];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchedObjects = [[[PPMDataStore sharedData]mainObjectContext]executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"error in %s with - %@", __PRETTY_FUNCTION__, error);
    }
    return fetchedObjects;
}

@end
