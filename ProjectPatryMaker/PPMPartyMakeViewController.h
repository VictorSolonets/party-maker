//
//  PPMPartyMakeViewController.h
//  ProjectPatryMaker
//
//  Created by 2 on 2/8/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPMEntityParty.h"
#import "PPMMapViewController.h"
#import "PPMAnnotationParty.h"

@interface PPMPartyMakeViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIButton *btnDateChoosing;
@property (nonatomic, weak) IBOutlet UITextField *partyName;
@property (nonatomic, weak) IBOutlet UISlider *partySliderMinTime;
@property (nonatomic, weak) IBOutlet UISlider *partySliderMaxTime;
@property (nonatomic, weak) IBOutlet UITextView *partyDescription;
@property (nonatomic, weak) IBOutlet UIScrollView *partyScrollView;
@property (nonatomic, weak) IBOutlet UIPageControl *partyPageControl;
@property (nonatomic, weak) IBOutlet UIImageView *image;

@property (nonatomic) PPMMapViewController *mapView;
@property (nonatomic) PPMAnnotationParty *annotation;

@property (nonatomic) NSNumber *currentPicture;
@property (nonatomic) NSNumber *partyID;

@property (nonatomic) NSString *subtitle;
@property (nonatomic) NSString *location;

- (void) setParty: (NSNumber*) partyID;

@end
