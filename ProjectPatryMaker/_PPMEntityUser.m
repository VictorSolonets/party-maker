// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PPMEntityUser.m instead.

#import "_PPMEntityUser.h"

const struct PPMEntityUserAttributes PPMEntityUserAttributes = {
	.email = @"email",
	.name = @"name",
	.userID = @"userID",
};

const struct PPMEntityUserRelationships PPMEntityUserRelationships = {
	.parties = @"parties",
};

@implementation PPMEntityUserID
@end

@implementation _PPMEntityUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PPMEntityUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PPMEntityUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PPMEntityUser" inManagedObjectContext:moc_];
}

- (PPMEntityUserID*)objectID {
	return (PPMEntityUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"userIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic email;

@dynamic name;

@dynamic userID;

- (int64_t)userIDValue {
	NSNumber *result = [self userID];
	return [result longLongValue];
}

- (void)setUserIDValue:(int64_t)value_ {
	[self setUserID:@(value_)];
}

- (int64_t)primitiveUserIDValue {
	NSNumber *result = [self primitiveUserID];
	return [result longLongValue];
}

- (void)setPrimitiveUserIDValue:(int64_t)value_ {
	[self setPrimitiveUserID:@(value_)];
}

@dynamic parties;

- (NSMutableSet*)partiesSet {
	[self willAccessValueForKey:@"parties"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"parties"];

	[self didAccessValueForKey:@"parties"];
	return result;
}

@end

