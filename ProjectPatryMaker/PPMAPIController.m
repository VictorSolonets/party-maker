//
//  PPMAPIController.m
//  ProjectPatryMaker
//
//  Created by 2 on 2/22/16.
//  Copyright © 2016 Victor Solonets. All rights reserved.
//

#import "PPMAPIController.h"
#import "PPMDataStore.h"
#import "PPMPartyMakerSDK.h"
#import "PPMPartyNotification.h"

@implementation PPMAPIController

+ (instancetype) sharedInstance {
    static PPMAPIController *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PPMAPIController alloc] init];
    });
    return instance;
}


- (void)ppm_callBlockOnMainThread:(void (^) (NSDictionary* response, NSError *error))block responce:(NSDictionary*)responceDict error:(NSError*)error {
    dispatch_async( dispatch_get_main_queue(), ^{
        if (block) {
            block(responceDict, error);
        }
    });
}

- (void) getAllPartiesFromUserID:(NSNumber*)userID {
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    [[PPMPartyMakerSDK sharedManager] getAllPartyFromUser:userID Callback:^(NSDictionary *response, NSError *error) {
        if (![[[response objectForKey:@"response"] class] isSubclassOfClass: [NSNull class]]) {
            for (NSDictionary *allParties in [response objectForKey:@"response"]) {
                [[PPMDataStore sharedData] addPartyWithName:[allParties objectForKey:@"name"]
                                                     LogoID:[f numberFromString:[allParties objectForKey:@"logo_id"]]
                                                   Latitude:[allParties objectForKey:@"latitude"]
                                                  Longitude:[allParties objectForKey:@"longitude"]
                                                    Comment:[allParties objectForKey:@"comment"]
                                                  TimeStart:[f numberFromString:[allParties objectForKey:@"start_time"] ]
                                                    TimeEnd:[f numberFromString:[allParties objectForKey:@"end_time"] ]
                                                    PartyID:[f numberFromString:[allParties objectForKey:@"id"] ]
                                                  CreatorID:[f numberFromString:[allParties objectForKey:@"creator_id"] ]
                                                 completion:^{}];
            }
        }
    }];
}


@end
